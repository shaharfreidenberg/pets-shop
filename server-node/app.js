const express = require("express");
var cors = require("cors");
const bodyParser = require("body-parser");
const petTypes = require("./routes/petType");
const productTypes = require("./routes/productType");
const products = require("./routes/product");
const orders = require("./routes/order");
const users = require("./routes/user");
const statistics = require("./routes/statistics");
const dbconfig = require("./config/db.config");
var mongoose = require("mongoose");

var app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connect to DB
mongoose.connect(dbconfig.connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use("/products", products);
app.use("/orders", orders);
app.use("/petTypes", petTypes);
app.use("/productTypes", productTypes);
app.use("/users", users);
app.use("/statistics", statistics);

app.use(express.static('public'));

app.listen(8080);

var http = require("http");
var socketIo = require("socket.io");
var server = http.createServer(app);

const io = socketIo(
  server /*, {
    cors: {
        origins: ['http://localhost:4200', 'http://locahost:3000'],
        methods: ['GET', 'POST'],
        credentials: false
    }
}*/
);

var count = 0;
io.on("connection", (socket) => {
  if (socket.handshake.headers.origin == "http://localhost:3000") {
    count++;
    socket.broadcast.emit("connected users count", count);
  }
  socket.on("disconnect", () => {
    if (socket.handshake.headers.origin == "http://localhost:3000") {
      count--;
      socket.broadcast.emit("connected users count", count);
    }
  });
});

module.exports.ioObj = io;

server.listen(8081);
