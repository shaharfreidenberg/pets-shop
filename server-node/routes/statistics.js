const express = require('express');
const statisticsController = require('../controllers/statisitics');
var router = express.Router();

router.get('/monthlyIncomes', statisticsController.getMonthlyIncomes);
router.get('/registeredUsersCount', statisticsController.getRegisteredUseresCount);
router.get('/productsCount', statisticsController.getProductsCount);
router.get('/ordersCount', statisticsController.getOrdersCount);

module.exports = router;