const express = require("express");
const userController = require("../controllers/user");
const { body } = require("express-validator");
var router = express.Router();

router.post(
  "/register",
  [
    body("password", "Password must contain at least 8 characters").isLength({
      min: 8,
    }),
    body("email", "Please enter a valid email")
      .isEmail()
      .bail()
      .custom((value) => {
        return userController.findUserByEmail(value).then((user) => {
          if (user) {
            return Promise.reject("Email is already in use");
          }
        });
      }),
  ],
  userController.register
);
router.post("/login", userController.login);
router.post("/admin/login", userController.loginAdmin);
router.put(
  "/",
  [
    body("id").custom((value) => {
      return userController.findUserById(value).then((user) => {
        if (!user) {
          return Promise.reject("User does not exist");
        }
      });
    }),
    body("password", "Password must contain at least 8 characters").isLength({
      min: 8,
    }),
    body("email", "Please enter a valid email")
      .isEmail()
      .bail()
      .custom((value, { req }) => {
        return userController.findUserByEmail(value).then((user) => {
          if (user && user.id != req.body.id) {
            return Promise.reject("Email is already in use");
          }
        });
      }),
  ],
  userController.update
);
router.put("/shoppingCart", userController.addProductToShoppingCart);
router.put("/changePermissions", userController.changePermissions);
router.get("/cartItems/:userId", userController.getCartItems);
router.get("/all", userController.getAllUsers);
router.delete(
  "/",
  [
    body("id").custom((value) => {
      return userController.findUserById(value).then((user) => {
        if (!user) {
          return Promise.reject("User does not exist");
        }
      });
    }),
  ],
  userController.remove
);

router.get("/shoppingCart/getCart/:userid", userController.getShoppingCart);

router.get("/shoppingCart/:userid", userController.findCartByUserId);
router.put(
  "/shoppingCart/remove/one",
  userController.removeProductFromShoppingCart
);
router.put(
  "/shoppingCart/remove/All",
  userController.removeAllProductFromShoppingCart
);

router.put(
  "/shoppingCart/removeShoppingCart",
  userController.removeUserShoppingCart
);

module.exports = router;
