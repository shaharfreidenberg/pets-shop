const express = require('express');
const petTypeController = require('../controllers/petType');
var router = express.Router();

router.get('/', petTypeController.get);
router.get('/name/:typeName', petTypeController.getByName);
router.get('/:typeId', petTypeController.getById);
router.post('/', petTypeController.create);
router.put('/', petTypeController.update);
router.delete('/:petTypeId', petTypeController.remove);

module.exports = router;