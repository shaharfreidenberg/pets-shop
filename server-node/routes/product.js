const express = require('express');
const productController = require('../controllers/product');
var multer = require("multer");
var path = require('path');
var router = express.Router();

// set product image upload configuration
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname,'../public/img'))
    },
    filename: function (req, file, cb) {
      cb(null, req.body.id + path.extname(file.originalname))
    }
})
var upload = multer({ storage: storage })

router.get('/', productController.get);
router.get('/pets/:petTypeId', productController.getByPetTypeId);
router.get('/types/:productTypeId', productController.getByProductTypeId);
router.get('/search/:query', productController.getByQuery);
router.get('/all', productController.getAll);
router.get('/picture/:productId', productController.getProductPicture);
router.get('/filter', productController.managerFilter);
router.get('/import', productController.scrape);
router.put('/', productController.update);
router.put('/picture', upload.single("image"), productController.uploadImage);
router.delete('/:productId', productController.remove);
router.post('/', productController.create);

module.exports = router;