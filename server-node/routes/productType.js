const express = require('express');
const productTypeController = require('../controllers/productType');
var router = express.Router();

router.get('/', productTypeController.get);
router.get('/all', productTypeController.getAll);
router.get('/name/:typeName', productTypeController.getByName);
router.get('/petType/:petTypeId', productTypeController.getByPet);
router.get('/:TypeId', productTypeController.getById);
router.post('/', productTypeController.create);
router.put('/', productTypeController.update);
router.delete('/:productTypeId', productTypeController.remove);

module.exports = router;