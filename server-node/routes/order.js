const express = require("express");
const orderController = require("../controllers/order");
var router = express.Router();

router.get("/previous/:userId", orderController.getPrevious);
router.post("/create", orderController.create);
router.get("/", orderController.getAllOrders);
router.put("/status", orderController.setOrderStatus);
router.get('/:orderId', orderController.getOrderById);

module.exports = router;
