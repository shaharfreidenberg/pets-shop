const productType = require('../models/productType');
const ProductType = require('../models/productType');
const petType = require('../models/petType');
const product = require('../models/product');
const mongoose = require('mongoose');

const create = async (req, res) => {
    var qPetType = await petType.findById(req.body.petType);
    var sameProductType = await productType.findOne({
        'name': new RegExp(`^${req.body.name}$`, "i"),
        'petType': req.body.petType
    });

    if(!qPetType) {
        res.status(404).send({ message: "Pet type does not exist"});
    }
    else if (sameProductType) {
        res.status(400).send({ message: "Product type already exist for this pet type"});
    }
    else {
        const newProductType = new productType( {
            name: req.body.name,
            petType: mongoose.Types.ObjectId(req.body.petType)
        });
    
        await newProductType.save()
        res.status(200).send({ message: "Product type was created successfully" });
    }
};

const get = (req, res) => {
    ProductType.find().then(results => {
        res.json(results);
    });
};


const getByName = (req, res) => {
    ProductType.find({
        'name': {
            $regex: `.*${req.params.TypeName}.*`
        }
    }).then(productType => {
        res.json(productType);
    });
};

const getByPet = (req, res) => {
    ProductType.find({
        'petType': req.params.petTypeId
    }).sort({name: 1})
    .then(productType => {
        res.json(productType);
    });
};

const getById = (req, res) => {
    ProductType.findById(req.params.TypeId)
        .then(productType => {
            res.json(productType);
        });
}

const update = async (req, res) => {
    var updProductType = await productType.findById(req.body._id);
    var sameProductType = await productType.findOne({
        'name': new RegExp(`^${req.body.name}$`, "i"),
        'petType': req.body.petType
    });

    if(!updProductType) {
        res.status(404).send({ message: "Product type does not exist" })
    }
    else if(sameProductType){
        res.status(400).send({ message: "Product type already exist for this pet type"})
    }
    else {
        updProductType.name = req.body.name;
        updProductType.petType = mongoose.Types.ObjectId(req.body.petType);
        await updProductType.save();
        res.status(200).send({ message: "Product type was updated successfully" });
    }
}

const remove = async (req, res) => {
    var delProductType = await productType.findById(req.params.productTypeId);

    if (!delProductType) {
        res.status(404).send({ message: "Product type does not exist" });
    }
    else {
        await product.remove({ productType: delProductType._id });
        await delProductType.remove();
        res.status(200).send({ message: "Product type was deleted successfully" })
    }
}

const getAll = async (req, res) => {
    var productTypes = await productType.find({}).populate('petType');
    res.status(200).send(productTypes)
};

module.exports = {create, get, getByName, update, remove, getById, getByPet, getAll};