const order = require('../models/order');
const product = require('../models/product');
const user = require('../models/user');

const getMonthlyIncomes = (req, res) => {
    var pipeline = [
        {
            "$project":
            {
              "month": { "$month": "$date" },
              "year": { "$year": "$date" },
              "products": 1
            }
        },
        {
            "$lookup": {
                "from": "products",
                "localField": "products",
                "foreignField": "_id",
                "as": "products"
            }
        },
        {
            "$unwind": "$products"
        },
        {
            "$group":{
                "_id" : {
                    "month": "$month",
                    "year": "$year"
                },
                "income": { 
                    "$sum": "$products.price" 
                } 
            }
        },
        { "$sort" : { "_id.year" : 1,  "_id.month": 1 } }
    ];
    
    order.aggregate(pipeline)
    .then(results => {
        res.json(results);
        console.log(results)
    });
};

const getRegisteredUseresCount = (req, res) => {
    user.count()
    .then(results => {
        res.json(results);
    });
};

const getProductsCount = (req, res) => {
    product.count()
    .then(results => {
        res.json(results);
    });
};

const getOrdersCount = (req, res) => {
    order.count()
    .then(results => {
        res.json(results);
    });
};
module.exports = { getMonthlyIncomes, getRegisteredUseresCount, getProductsCount, getOrdersCount };