const petType = require('../models/petType');
const product = require('../models/product');
const productType = require('../models/productType');
var mongoose = require('mongoose');

const create = async (req, res) => {
    var samePetType = await petType.findOne({
        'name': new RegExp(`^${req.body.name}$`, "i")
    });

    if (samePetType) {
        res.status(400).send({ message: "Pet type already exist"})
    }
    else {
        const newPetType = new petType( {
            name: req.body.name
        });
    
        await newPetType.save()
        res.status(200).send({ message: "Pet type was created successfully" });
    }
};

const get = (req, res) => {
    petType.find().sort({name: 1})
    .then(results => {
        res.json(results);
    });
};


const getByName = (req, res) => {
    petType.find({
        'name': {
            $regex: `.*${req.params.TypeName}.*`
        }
    })
        .then(petType => {
            res.json(petType);
        });
};

const getById = async (req, res) => {
    var getPetType = await petType.findById(req.params.typeId);

    if (!getPetType) {
        res.status(404).send({ message: "Pet type not found" });
    }

    res.status(200).send(getPetType);
}

const update = async (req, res) => {
    var updPetType = await petType.findById(req.body._id);
    var samePetType = await petType.findOne({
        'name': new RegExp(`^${req.body.name}$`, "i")
    })

    if(!updPetType) {
        res.status(404).send({ message: "Pet type does not exist" })
    }
    else if(samePetType){
        res.status(400).send({ message: "Pet type already exist"})
    }
    else {
        updPetType.name = req.body.name;
        await updPetType.save();
        res.status(200).send({ message: "Pet type was updated successfully" });
    }
    
}

const remove = async (req, res) => {
    var delPetType = await petType.findById(req.params.petTypeId);

    if (!delPetType) {
        res.status(404).send({ message: "Pet type does not exist" });
    }
    else {
        await product.remove({ petType: delPetType._id });
        await productType.remove({ petType: delPetType._id });
        await delPetType.remove();
        res.status(200).send({ message: "Pet type was deleted successfully" })
    }


}

module.exports = {create, get, getByName, update, remove, getById};