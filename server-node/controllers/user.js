const user = require("../models/user");
const product = require("../models/product");
const { validationResult } = require("express-validator");
const bcryptjs = require("bcryptjs");
const app = require("../app");

const findUserByEmail = (email) => {
  return user.findOne({
    email: email,
  });
};

const findUserById = (id) => {
  return user.findById(id);
};

const register = (req, res) => {
  // validate parameters
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({
      errors: errors.array(),
    });
  }

  const newUser = new user({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: bcryptjs.hashSync(req.body.password, 8),
  });

  newUser
    .save()
    .then((user) => {
      res.status(200).send({
        id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
      });
      app.ioObj.emit("new registered user");
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ message: "Error in Saving" });
    });
};

const loginHelper = async (email, password) => {
  const authUser = await user.findOne({ email: email });

  if (!authUser || !bcryptjs.compareSync(password, authUser.password)) {
    return null;
  } else {
    return authUser;
  }
};

const login = async (req, res) => {
  const result = await loginHelper(req.body.email, req.body.password);

  if (!result) {
    res.status(400).send({ message: "User or password is incorrect" });
  } else {
    res.status(200).send({
      id: result._id,
      firstName: result.firstName,
      lastName: result.lastName,
      email: result.email,
    });
  }
};

const loginAdmin = async (req, res) => {
  const result = await loginHelper(req.body.email, req.body.password);

  if (!result) {
    res.status(400).send({ message: "Username or password is incorrect !" });
  } else if (!result.isAdmin) {
    res.status(403).send({ message: "User does not have rights on this page" });
  } else {
    res.status(200).send({
      id: result._id,
      firstName: result.firstName,
      lastName: result.lastName,
      email: result.email,
    });
  }
};

const update = (req, res) => {
  // validate parameters
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({
      errors: errors.array(),
    });
  }

  user
    .findById(req.body.id)
    .then((user) => {
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.email = req.body.email;
      user.password = bcryptjs.hashSync(req.body.password, 8);
      user.shoppingCart = user.shoppingCart;

      user
        .save()
        .then((user) => {
          res.status(200).send({
            id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
          });
        })
        .catch((err) => {
          console.log(err);
          res.status(500).send({ message: "Error in Saving" });
        });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ message: "Error in Saving" });
    });
};

const addProductToShoppingCart = (req, res) => {
  product.findById(req.body.productId).then((product) => {
    user
      .findOneAndUpdate(
        { _id: req.body.userId },
        {
          $push: {
            shoppingCart: {
              $each: [product],
            },
          },
        }
      )
      .then(() => res.status(200).send({ message: "success" }))
      .catch(() => res.status(500).send({ message: "failed" }));
  });
};

const getCartItems = (req, res) => {
  user
    .findById(req.params.userId)
    .then((user) => {
      res.status(200).send({
        cartItems: user.shoppingCart.length,
      });
    })
    .catch(() => res.status(500).send({ message: "failed" }));
};

const remove = (req, res) => {
  // validate parameters
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({
      errors: errors.array(),
    });
  }

  user
    .findByIdAndDelete(req.body.id, (err, user) => {
      if (err) {
        res.status(500).send({ message: "Failed to delete account" });
      } else {
        res.status(200).send({ message: "success" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ message: "Failed to delete account" });
    });
};

const getAllUsers = async (req, res) => {
  var users = await user.find({}).select("-password").select("-shoppingCart");
  res.status(200).send(users);
};

const changePermissions = async (req, res) => {
  var oldUser = await user.findById(req.body.id);

  if (!oldUser) {
    res.status(500).send({ message: "User does not exist" });
  } else {
    oldUser.isAdmin = !oldUser.isAdmin;
    var savedUser = await oldUser.save();
    res.status(200).send({
      id: savedUser._id,
      firstName: savedUser.firstName,
      lastName: savedUser.lastName,
      email: savedUser.email,
      isAdmin: savedUser.isAdmin,
    });
  }
};

const findCartByUserId = async (req, res) => {
  // try {
  const result = await user.findById(req.params.userid);
  let products = await product.find();
  copyProducts = [];

  const { shoppingCart } = result;

  let cart = [];
  shoppingCart.forEach(
    (productcart) => (cart[productcart] = (cart[productcart] || 0) + 1)
  );

  products.forEach((product, index) => {
    copyProducts.push({ ...product._doc });
    copyProducts[index]["amount"] = cart[product._id];
  });

  copyProducts = copyProducts.filter((product) => product.amount >= 1);

  res.json(copyProducts);
  //res.status(200).send({ message: "success" });
  // } catch (e) {
  //   console.log(e);
  //   //  res.status(500).send({ message: "failed" });
  // }
};

const removeProductFromShoppingCart = async (req, res) => {
  try {
    const curruser = await user.findById(req.body.userId);
    const { shoppingCart } = curruser;
    const index = shoppingCart.indexOf(req.body.productId);
    shoppingCart.splice(index, 1);
    curruser.save();
    res.status(200).send({ message: "success" });
  } catch {
    res.status(500).send({ message: "failed" });
  }
};

const removeUserShoppingCart = async (req, res) => {
  try {
    const curruser = await user.findById(req.body.userId);
    const { shoppingCart } = curruser;
    shoppingCart.splice(0, shoppingCart.length);
    curruser.save();
    res.status(200).send({ message: "success" });
  } catch {
    res.status(500).send({ message: "failed" });
  }
};

const removeAllProductFromShoppingCart = async (req, res) => {
  try {
    const curruser = await user.findById(req.body.userId);
    const { shoppingCart } = curruser;
    shoppingCart.pull(req.body.productId);
    curruser.save();
    res.status(200).send({ message: "success" });
  } catch {
    res.status(500).send({ message: "failed" });
  }
};

const getShoppingCart = async (req, res) => {
  try {
    const result = await user.findById(req.params.userid);
    res.json(result.shoppingCart);
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  register,
  findUserByEmail,
  addProductToShoppingCart,
  login,
  update,
  findUserById,
  remove,
  getCartItems,
  loginAdmin,
  getAllUsers,
  changePermissions,
  removeProductFromShoppingCart,
  removeUserShoppingCart,
  removeAllProductFromShoppingCart,
  getShoppingCart,
  findCartByUserId,
};
