const product = require('../models/product');
const productType = require('../models/productType');
const petType = require('../models/petType');
let axios = require('axios');
let cheerio = require('cheerio');
var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
const app = require("../app");

const get = (req, res) => {
    product.find().sort({name: 1})
    .then(results => {
        res.json(results);
    });
};

const getByPetTypeId = (req, res) => {
    product.find({
        'petType': req.params.petTypeId
    }).sort({name: 1})
    .then(product => {
        res.json(product);
    });
};

const getByProductTypeId = (req, res) => {
    product.find({
        'productType': req.params.productTypeId
    }).sort({name: 1})
    .then(product => {
        res.json(product);
    });
};

const getByQuery = (req, res) => {
    product.find({
        'name': new RegExp(req.params.query, 'i')
    }).sort({name: 1})
        .then(product => {
            return res.json(product);
        });
};

const getAll = async (req, res) => {
    var products = await product.find({}).populate('productType').populate('petType');
    res.status(200).send(products);
};

const getProductPicture = async (req, res) => {
    var searchedProduct = await product.findById(req.params.productId);

    if (!searchedProduct) {
        res.status(404).send({ message: "Product not found" })
    }

    res.sendFile(path.join(__dirname, '../public/img', searchedProduct.picture));
};

const update = async (req, res) => {
    var newProduct = await product.findById(req.body._id);
    if (!newProduct) {
        res.status(404).send({ message: "Product not found" });
    }

    var qProductType = await productType.findById(req.body.productType).populate('petType')
    if (!qProductType) {
        res.status(404).send({ message: "Product type not found" });
    }
    else if (qProductType.petType._id != req.body.petType) {
        res.status(500).send({ message: "Product type does not match pet type" });
    }
    else {
        newProduct.name = req.body.name;
        newProduct.petType = qProductType.petType._id;
        newProduct.productType = qProductType._id;
        newProduct.price = req.body.price;
        newProduct.supplier = req.body.supplier
        newProduct.description = req.body.description;

        await newProduct.save();
        res.status(200).send({ message :"Product was updated successfully" });
    }
};

const uploadImage = async (req,res) => {
    var newProduct = await product.findById(req.body.id);

    if(!newProduct) {
        res.status(404).send({ message: "Product does not exist" });
    }

    newProduct.picture = req.body.id + path.extname(req.file.originalname);
    await newProduct.save()
    var action = (req.body.isCreate === 'true') ? "created" : "updated";
    res.status(200).send({ message: `Product was ${action} successfully` });
};

const remove = async(req, res) => {
    var delProduct = await product.findById(req.params.productId);

    if(!delProduct) {
        res.status(404).send({ message: "Product does not exist" });
    }

    await delProduct.remove()
    res.status(200).send({ message: "Product was deleted successfully" });
};

const create = async (req, res) => {
    var qProductType = await productType.findById(req.body.productType).populate('petType')
    var sameProduct = await product.find({
        'name': req.body.name,
        'supplier': req.body.supplier,
        'petType': qProductType.petType._id,
        'productType': qProductType._id
        });
        
    if (!qProductType) {
        res.status(404).send({ message: "Product type not found" });
    }
    else if (qProductType.petType._id != req.body.petType) {
        res.status(500).send({ message: "Product type does not match pet type" });
    }
    else if (sameProduct.length > 0) {
        res.status(500).send({ message: `Product ${sameProduct[0]._id} has the same name, supllier, pet type and product type.\n
                                         Please change one of the above fields and try again.`});
    }
    else {
        const newProduct = new product({
            name: req.body.name,
            petType: qProductType.petType._id,
            productType: qProductType._id,
            price: req.body.price,
            supplier: req.body.supplier,
            description: req.body.description,
        });

        await newProduct.save();
        app.ioObj.emit("new product");
        res.status(200).send(newProduct);
    }
};

const managerFilter = async (req,res) => {
    var filterId;
    try {
        filterId = mongoose.Types.ObjectId(req.query.textFilter.toString())
    }
    catch {
        filterId = null
    }

    var products = await product.find({ $and: [
        (req.query.textFilter) ? { $or: [
            { _id: filterId },
            { 'name': new RegExp('.*' + req.query.textFilter + '*.', 'i') }
        ]} : {},
        (req.query.petType) ? { petType: req.query.petType } : {},
        (req.query.productType) ? { productType: req.query.productType } : {}
    ]}).populate('productType').populate('petType');

    res.status(200).send(products);
}

const scrape = async (req, res) => {  
    petTypes = [];
    productTypes = [];
    products = [];
    var localUrl = "http://localhost:8080"
    const page = await axios.get('https://www.petsmart.com/');
    const $ = cheerio.load(page.data);

    // Get pet types
    $('a._GN_petTypeContainer__typeCopy').each(function() {
        var petType = $(this).text();
        petTypes.push(petType);
        $('li._GN_petTypeContainer__linkCategory').each(function() {
            $('a', this).each(function() {
                var url = $(this).attr('href');
                if(url.split('/').indexOf(petType.replace(' ', '-')) > -1) { 
                    // Get product types
                    var productType = $(this).text()
                    productTypes.push({ petType: petType, productType: productType, url: url });
                }
            })
        })
    });

    // Add pet types to DB
    for(var i in petTypes) {
        try {
            var newPetType = petTypes[i];
            newPetType = newPetType[0].toUpperCase() + newPetType.substring(1);
            await axios.post(localUrl + "/petTypes", { name: newPetType });
        }
        catch(err) {
            // Pet type already exist
            //console.log(err.response.data.message);
        }
    }

    for(var i in productTypes) {
        var petTypeName = productTypes[i].petType[0].toUpperCase() + productTypes[i].petType.substring(1);
        var petTypeId = (await petType.findOne({name: petTypeName}))._id;

        // Add product type to DB
        try {
            await axios.post(localUrl + "/productTypes", { 
                name: productTypes[i].productType, 
                petType: petTypeId
            });
        }
        catch(err) {
            // Product type already exist for this pet type
            //console.log(err.response.data.message);
        }

        var suppliers = [];
        var productsPage = await axios.get(productTypes[i].url);
        const $p = cheerio.load(productsPage.data);

        // Get all suppliers
        $p('div.refinement.brand a.refinement-checkbox').each(function() {
            suppliers.push($(this).text().split('\n')[1])
        })

        // Get products
        $p('a.name-link').each(function() {
            var src = $p('div.product-image img', this).attr('src');
            var name = $p('div.product-name h3', this).text();

            var price
            try {
                price = $p('span.price-standard', this).text().split('\n')[1].substring(1);
            }
            catch {
                price = $p('span.price-regular', this).text().split('\n')[1].substring(1);
            }

            var supplier;
            suppliers.forEach(sup => (name.split('®').join('').split('™').join('').toLowerCase().includes(sup.toLowerCase())) ? supplier = sup : null);
            
            // Fix original site's spelling mistakes
            if(name.includes('Omega™One') || name.includes('Omega™ Betta')) {
                supplier = "Omega One";
            }
            else if (name.includes('TetroPro™')) {
                supplier = "Tetra";
            }
            else if(name.includes('Grreat Choice®')) {
                supplier = "Great Choice";
            }
            // no supplier means it's a live animal
            else if(!supplier) {
                supplier = "Pet Smart"
            }

            products.push({ name: name, 
                            price: price, 
                            supplier: supplier, 
                            petType: petTypeId, 
                            productType: productTypes[i].productType, 
                            description: name,
                            img: src 
            });
        })
    }

    // Add products to DB
    for(var i in products) {
        var newProduct = products[i];
        var productTypeId = (await productType.findOne({
            name: newProduct.productType,
            petType: newProduct.petType
        }))._id;

        try {
            var savedProduct = await axios.post(localUrl + "/products", {
                name: newProduct.name,
                price: newProduct.price,
                description: newProduct.description,
                supplier: newProduct.supplier,
                petType: newProduct.petType,
                productType: productTypeId,
            });

            axios({ method: 'get', url: newProduct.img, responseType:'stream' })
            .then(res => {
                res.data.pipe(fs.createWriteStream(path.join(__dirname,'../public/img', `${savedProduct.data._id}.png`)));
            })

            var updProduct  = await product.findById(savedProduct.data._id);
            updProduct.picture = `${savedProduct.data._id}.png`;
            await updProduct.save();
        }
        catch(err) {
            // Product already exist
            //console.log(err.response.data.message);
        }
    }

    res.status(200).send({ message: " Products were imported successfully" })
};

module.exports = { get, getByPetTypeId, getByProductTypeId, getByQuery, getAll, getProductPicture, update, uploadImage, remove, create, managerFilter, scrape };