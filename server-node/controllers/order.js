const order = require("../models/order");
const app = require("../app");

const getPrevious = (req, res) => {
  order
    .find({
      userId: req.params.userId,
    })
    .sort({ date: -1 })
    .populate("products")
    .then((orders) => {
      res.json(orders);
    });
};

//shiri
const create = (req, res) => {
  const newOrder = new order({
    userId: req.body.userId,
    products: req.body.products,
    address: req.body.address,
    city: req.body.city,
    phone: req.body.phone,
    isShipped: false,
  });

  newOrder
    .save()
    .then((order) => {
      // socketio
      app.ioObj.emit('new order', order._id);
      res.status(200).json(newOrder);
    })
    .catch((err) => {
      res.status(500).json({ message: "Failed" });
    });
};

const getAllOrders = (req, res) => {
  order
    .find()
    .sort({ date: -1 })
    .populate("products")
    .then((orders) => {
      res.json(orders);
    });
};

const setOrderStatus = async (req, res) => {
  const filter = { _id: req.body.orderId };
  const update = { isShipped: req.body.isShipped };
  await order.findOneAndUpdate(filter, update)
    .then(() => {
      app.ioObj.emit('order status changed', 
        { orderId: req.body.orderId, isShipped: req.body.isShipped} );
    });
};

const getOrderById = (req, res) => {
  order.findById(req.params.orderId)
  .populate("products")
  .then((order) => {
    res.json(order);
  });
};


module.exports = {
  getPrevious,
  getAllOrders,
  setOrderStatus,
  create,
  getOrderById
};
