const mongoose = require('mongoose');

const User = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type : Boolean,
        default: false
    },
    shoppingCart: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }],
});

module.exports = mongoose.model('User', User);