const mongoose = require('mongoose');

const ProductType = new mongoose.Schema({
    name:String,
    petType:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'petType',
        required: true
    },
});

module.exports = mongoose.model('productType', ProductType);