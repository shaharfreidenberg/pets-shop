const mongoose = require('mongoose');

const petType = new mongoose.Schema({
    name:String
});

module.exports = mongoose.model('petType', petType);