const mongoose = require('mongoose');

const Product = new mongoose.Schema({
    name:String,
    productType:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'productType',
        required: true
    },
    petType:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'petType',
        required: true
    },
    supplier:String,
    picture:String,
    description:String,
    price:Number
});

module.exports = mongoose.model('Product', Product);