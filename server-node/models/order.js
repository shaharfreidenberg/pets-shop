const mongoose = require('mongoose');

const Order = new mongoose.Schema({
    userId: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    products:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Product'
    }],
    address:String,
    city:String,
    phone:String,
    date:{
        type: Date,
        default: Date.now
    },
    isShipped: Boolean
});

module.exports = mongoose.model('Order', Order);