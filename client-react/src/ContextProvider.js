import { Context } from './Context';
import React from 'react';

export default function ContextProvider(props) {
  const [context, setContext] = React.useState({search: '', petTypeId: undefined, productTypeId: undefined, user: undefined, cartItems: 0});
  return (
    <Context.Provider value={[context, setContext]}>
        {props.children}
    </Context.Provider>
  );
}
