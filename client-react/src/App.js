import React from "react";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import "./App.css";
import PreviousOrders from "./modules/previous-orders/PreviousOrders";
import Header from "./modules/header/Header";
import Footer from "./modules/footer/Footer";
import ProductsWithFilter from "./modules/products/ProductsWithFilter";
import MyAccount from "./modules/my-account/MyAccount";
import ContextProvider from "./ContextProvider";
import { Container } from "@material-ui/core";
import ShoppingCart from "./modules/shopping-cart/ShoppingCart";
import Checkout from "./modules/checkout/Checkout";
import { socketContext, socket } from "./Context";

export default function App() {
  return (
    <socketContext.Provider value={socket}>
      <ContextProvider>
      <BrowserRouter>
        <Header />
        <Container>
          <Route path='/' exact>
            <Redirect to='/products' />
          </Route>
          <Route path='/products' exact component={ProductsWithFilter} />
          <Route path='/previous-orders' exact component={PreviousOrders} />
          <Route path='/my-account' exact component={MyAccount} />
          <Route path='/shopping-cart' exact component={ShoppingCart} />
          <Route path='/checkout' exact component={Checkout} />
        </Container>
        <Footer />
      </BrowserRouter>
    </ContextProvider>
    </socketContext.Provider>
  );
}