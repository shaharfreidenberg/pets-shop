import React from 'react';
import socketio from "socket.io-client";

const Context = React.createContext();
const socket = socketio.connect("http://localhost:8081");
const socketContext = React.createContext();

export { Context, socket, socketContext };