import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ShoppingCartItem from "../shopping-cart/ShoppingCartItem";
import { List, Card, Typography, Divider, Button } from "@material-ui/core";
import checkout from "../checkout/Checkout";
import { useHistory } from "react-router-dom";
import { Context } from "../../Context";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
    padding: theme.spacing(0, 3),
  },
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
  },
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    fontSize: "30px",
  },
  price: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    fontSize: "20px",
  },
}));

export default function ShoppingCart() {
  let lastState = undefined;
  const classes = useStyles();
  const [totalPrice, setTotalPrice] = React.useState(0);
  const [cart, setcart] = React.useState([]);
  const history = useHistory();
  const [context, setContext] = React.useContext(Context);
  const newContext = Object.assign({}, context);

  //const [price, setprice] = React.useState(0);
  React.useEffect(() => {
    if (JSON.parse(localStorage.getItem("user")) === null) {
      history.push("/products");
    } else {
      fetch(
        `http://localhost:8080/users/ShoppingCart/${
          JSON.parse(localStorage.getItem("user")).id
        }`
      )
        .then((response) => response.json())
        .then((data) => setcart(data));
    }
  }, []);

  if (cart.length === 0)
    return (
      <Typography className={classes.title}>Your cart is empty</Typography>
    );
  else {
    if (totalPrice === 0) calculateTotalPrice();
  }

  function deleteProduct(id) {
    setcart(cart);
    let index = cart.findIndex((product) => product._id === id);
    let arr = cart;
    newContext.cartItems = newContext.cartItems - arr[index].amount;
    setContext(newContext);
    arr[index].amount = 0;
    arr.splice(index, 1);
    setcart([]);
    setcart(arr);
    calculateTotalPrice();
  }

  function removeProduct(id) {
    setcart(cart);
    console.log(id);
    let index = cart.findIndex((product) => product._id === id);
    let arr = cart;
    arr[index].amount--;
    newContext.cartItems = newContext.cartItems - 1;
    setContext(newContext);
    setcart([]);
    setcart(arr);
    calculateTotalPrice();
  }

  function addProduct(id) {
    let index = cart.findIndex((product) => product._id === id);
    let arr = cart;
    arr[index].amount++;
    newContext.cartItems = newContext.cartItems + 1;
    setContext(newContext);
    setcart(arr);
    calculateTotalPrice();
  }

  function calculateTotalPrice() {
    let totalPrice = 0;

    if (cart.length !== 0)
      cart.forEach((product) => (totalPrice += product.price * product.amount));

    setTotalPrice(totalPrice);
  }

  return (
    <div className={classes.root}>
      <Typography className={classes.title}>Shopping Cart</Typography>
      <Divider />
      <List>
        {cart.map((product, index) => (
          <ShoppingCartItem
            name={product.name}
            price={product.price}
            amount={product.amount}
            picture={`http://localhost:8080/img/${product.picture}`}
            id={product._id}
            key={index}
            onDelete={deleteProduct}
            onRemove={removeProduct}
            onAdd={addProduct}
          />
        ))}
      </List>

      <Divider />
      <Typography className={classes.price}>
        {"Total Price: " + totalPrice.toFixed(2) + " $"}
      </Typography>
      <Button
        variant='contained'
        color='primary'
        disableElevation
        variant='contained'
        color='primary'
        fullWidth
        onClick={() => {
          history.push("/checkout", { cart: cart, totalPrice: totalPrice });
        }}
      >
        Checkout
      </Button>
    </div>
  );
}
