import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: "30ch",
    backgroundColor: theme.palette.background.paper,
    color: "black",
  },
  inline: {
    display: "inline",
  },
  Divider: {
    width: "100%",
    maxWidth: 360,
  },
}));

export default function ShoppingCartItem(props) {
  const classes = useStyles();
  const [itemAmount, setAmount] = useState(props.amount);

  function handleAdd() {
    setAmount(itemAmount + 1);
    console.log(props.id);
    fetch("http://localhost:8080/users/shoppingCart", {
      method: "PUT",
      body: JSON.stringify({
        productId: props.id,
        userId: JSON.parse(localStorage.getItem("user")).id,
      }),
      headers: { "Content-Type": "application/json" },
    }).then(console.log(Response));

    props.onAdd(props.id);
  }

  function handleRemove() {
    if (itemAmount !== 0 && itemAmount !== 1) {
      setAmount(itemAmount - 1);
      fetch("http://localhost:8080/users/shoppingCart/remove/one", {
        method: "PUT",
        body: JSON.stringify({
          productId: props.id,
          userId: JSON.parse(localStorage.getItem("user")).id,
        }),
        headers: { "Content-Type": "application/json" },
      }).then(console.log(Response));
      props.onRemove(props.id);
    } else {
      handleDelete();
    }
  }

  function handleDelete() {
    fetch("http://localhost:8080/users/shoppingCart/remove/All", {
      method: "PUT",
      body: JSON.stringify({
        productId: props.id,
        userId: JSON.parse(localStorage.getItem("user")).id,
      }),
      headers: { "Content-Type": "application/json" },
    }).then(console.log(Response));

    setAmount(0);

    props.onDelete(props.id);
  }

  return (
    <div>
      <Paper className={classes.paper}>
        <Grid container wrap='nowrap' spacing={2}>
          <ListItem>
            <ListItemAvatar>
              <Avatar src={props.picture} />
            </ListItemAvatar>
            <ListItemText primary={props.name} secondary={props.price + " $"} />
            <IconButton onClick={handleRemove}>
              <RemoveIcon />
            </IconButton>
            <IconButton onClick={handleAdd}>
              <AddIcon />
            </IconButton>
            <ListItem>
              <ListItemText
                className={classes.ListItemText}
                secondary={itemAmount}
              />
            </ListItem>
          </ListItem>
          <IconButton onClick={handleDelete}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </Paper>
    </div>
  );
}
