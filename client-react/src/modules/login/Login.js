import { Button, TextField, } from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Context } from '../../Context';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#BCB9DD",
  },
}));

export default function Login() {
  const classes = useStyles();
  const [context, setContext] = React.useContext(Context);
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [loginError, setLoginError] = React.useState('');
  let loginUrl = 'http://localhost:8080/users/login';

  const handleSubmit = (event) => {
    event.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ 
        email: email,
        password: password
      })
    };

    fetch(loginUrl, requestOptions).then(response => {
      let json = response.json();
      if (response.ok) return json;
      else return json.then(err => {throw err});
    })
    .then(data => {
      const newContext = Object.assign({}, context);
      newContext.user = data;
      setContext(newContext);
      localStorage.setItem("user", JSON.stringify(data));
    })
    .catch(data => setLoginError(data.message));
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
        <TextField variant="outlined" margin="normal" required fullWidth name="email"  label="Email Address" id="email"  autoComplete="email"autoFocus 
          value={email} onInput={e=>setEmail(e.target.value)} error={loginError !== ""}/>
        <TextField variant="outlined" margin="normal" required fullWidth name="password" label="Password"  id="password" autoComplete="current-password" type="password"
          value={password} onInput={e=>setPassword(e.target.value)} error={loginError !== ""} helperText={loginError}/>
        <Button type="submit" fullWidth variant="contained" className={classes.submit} >
            Log In
        </Button>
    </form>
  );
}