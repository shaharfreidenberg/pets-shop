import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root:{
      paddingRight: theme.spacing(3),
      display:"flex",
      alignItems:"center"
    }
}));

export default function Info(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {props.icon}
      <small style={{paddingLeft: 5}}>{props.text}</small>
    </div>
  );
}