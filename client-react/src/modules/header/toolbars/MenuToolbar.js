import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Link, List, Toolbar } from '@material-ui/core';
import MenuItemDropDown from './MenuItemDropDown';

const useStyles = makeStyles((theme) => ({
    menu:{
        display:"flex",
        justifyContent:"left",
        paddingTop: 0,
        paddingBottom: 0,
    },
}));

export default function MenuToolbar() {
    const classes = useStyles();
    const [petTypes, setPetTypes] = React.useState([]);

    React.useEffect(() => {        
        fetch('http://localhost:8080/petTypes')
        .then((response) => response.json())
        .then((data) => setPetTypes(data));
    }, []);

    if (!petTypes.length)
    return "";

    return(
        <>
        <Toolbar style={{minHeight:48}}>
        <Container>
            <List className={classes.menu} aria-labelledby="main navigation">
                {petTypes.map((petType) => (
                    <MenuItemDropDown key={petType._id} name={petType.name} id={petType._id}/>
                ))}
            </List>
        </Container>
        </Toolbar>
        </>
    );
  }