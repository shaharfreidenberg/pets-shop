import React from 'react';
import { Button, Container, Link, Menu, MenuItem, Toolbar, withStyles } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Info from './Info';
import { Context } from '../../../Context';
import LoginRegister from './LoginRegister';
import PlaceIcon from '@material-ui/icons/Place';
import PhoneIcon from '@material-ui/icons/Phone';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex"
    },
    toolbar: {
        backgroundColor: "#b0acd4"
    },
    container: {
        display:"flex", 
        justifyContent: "space-between"
    },
    userInfo: {
        textTransform: 'none',
    },
  }));

  const StyledMenu = withStyles({
    paper: {
      border: '1px solid #d3d4d5',
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      {...props}
    />
  ));

export default function TopToolbar() {
    const classes = useStyles();
    const [context, setContext] = React.useContext(Context);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    React.useEffect(() => {
        const user = JSON.parse(localStorage.getItem("user"));
        const newContext = Object.assign({}, context);
        newContext.user = user;
        setContext(newContext);
    }, []);

    const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
    setAnchorEl(null);
    };

    const handleLogout = () => {
        setAnchorEl(null);
        const newContext = Object.assign({}, context);
        newContext.user = undefined;
        newContext.cartItems = 0;
        setContext(newContext);
        localStorage.removeItem("user");
    };

    return(
        <>
        <Toolbar variant="dense" className={classes.toolbar}>
            <Container className={classes.container}>
                <div className={classes.root}>
                    <Info icon={<PlaceIcon fontSize="small" />} text="Ben Yehuda 5, Tel Aviv"/>
                    <Info icon={<PhoneIcon fontSize="small" />} text="03-4060809"/>
                    <Info icon={<WhatsAppIcon fontSize="small" />} text="050-3200000"/>
                </div>
                {!context.user && 
                <div>
                    <LoginRegister action="login"/>
                    <LoginRegister action="register" variant="outlined" disableElevation/>
                </div>}
                {context.user && 
                <div>
                <Button edge="start" className={classes.userInfo} aria-label="account of current user" aria-controls="user-menu-appbar" aria-haspopup="true" onClick={handleMenu}> 
                    {`Hi! ${context.user.firstName} ${context.user.lastName}`} 
                    <ExpandMoreIcon fontSize="small" />
                </Button>
                <StyledMenu
                id="user-menu-appbar"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}>
                <MenuItem onClick={handleClose}>
                  <Link component={RouterLink} to="/my-account" underline="none" color="inherit">
                    My Account
                  </Link>
                </MenuItem>
                <MenuItem onClick={handleClose}>
                  <Link component={RouterLink} to="/previous-orders" underline="none" color="inherit">
                    Orders
                  </Link>
                </MenuItem>
                <MenuItem onClick={handleLogout}>Log Out</MenuItem>
              </StyledMenu>
                </div>}
            </Container>
        </Toolbar>
        </>
    );
}