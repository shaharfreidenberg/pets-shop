import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Link, ListItem, ListItemText, MenuItem, MenuList, Popover } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Context } from '../../../Context';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    menuItem: {
        color: "black",
        '&:hover': {
            color: "#a09bdd"
        }
    },
    dropdownItem: {
        color: "#777",
    },
    li : {
        paddingLeft: 0
    },
    root: {
        pointerEvents: 'none',
        
    },
    popoverContent: {
        pointerEvents: 'auto',
    },
}));

export default function MenuItemDropDown({name, id}) {   
    const classes = useStyles();
    const [productTypes, setProductTypes] = React.useState([]);
    const [openedPopover, setOpenedPopover] = React.useState(false)
    const popoverAnchor = React.useRef(null);
    const [context, setContext] = React.useContext(Context);
    const history = useHistory();

    const popoverEnter = ({ currentTarget }) => {
        setOpenedPopover(true)
    };

    const popoverLeave = ({ currentTarget }) => {
        setOpenedPopover(false);
    };

    function handleProductTypeClick(productTypeId) {
        const newContext = Object.assign({}, context);
        newContext.productTypeId = productTypeId;
        newContext.petTypeId = undefined;
        newContext.search = '';
        setContext(newContext);
        history.push('/products');
    }

    function handlePetTypeClick() {
        const newContext = Object.assign({}, context);
        newContext.productTypeId = undefined;
        newContext.search = '';
        newContext.petTypeId = id;
        setContext(newContext);
    }

    React.useEffect(() => {        
        fetch(`http://localhost:8080/productTypes/petType/${id}`)
        .then((response) => response.json())
        .then((data) => setProductTypes(data));
    }, []);

    return(
        <>
        <Link component={RouterLink} to={`/products`} underline="none" className={classes.menuItem} 
            onClick={handlePetTypeClick}
            ref={popoverAnchor}
            aria-owns="mouse-over-popover"
            aria-haspopup="true"
            onMouseEnter={popoverEnter}
            onMouseLeave={popoverLeave}>
        <ListItem className={classes.li}>
            <ListItemText primary={name} />
            <ExpandMoreIcon fontSize="small" />
        </ListItem>
        </Link>
        <Popover
            id="mouse-over-popover"
            className={classes.root}
            classes={{
                paper: classes.popoverContent
            }}
            open={openedPopover}
            anchorEl={popoverAnchor.current}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
            elevation={1}
            PaperProps={{onMouseEnter: popoverEnter, onMouseLeave: popoverLeave}}>
            <MenuList>
                {productTypes.map((productType) => (
                    <MenuItem key={productType._id} className={classes.dropdownItem} onClick={() => handleProductTypeClick(productType._id)}>
                        {productType.name}
                    </MenuItem>
                ))}
            </MenuList>
        </Popover>
        </>
    );
}