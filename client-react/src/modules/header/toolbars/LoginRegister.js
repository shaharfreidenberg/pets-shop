import React from 'react';
import { Avatar, Backdrop, Button, Container, CssBaseline, Fade, IconButton, makeStyles, Modal, Paper, Tab, Tabs, Typography } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Login from '../../login/Login';
import Register from '../../register/Register';

const useStyles = makeStyles((theme) => ({
    login: {
        color: "white",
        marginRight: theme.spacing(1),
    },
    register: {
        backgroundColor:"white", 
        color:"#b0acd4", 
        borderRadius:40,
        '&:hover':{
            backgroundColor: "rgba(255, 255, 255, 0.6)",
            color:"rgba(0, 0, 0, 0.8)"
        }
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '1px solid rgba(0,0,0,0.15)',
        borderRadius: 10,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    close: {
        float: "right",
    },
    form: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
    avatar: {
    margin: theme.spacing(1),
    backgroundColor: "#BCB9DD",
    },
}));

function TabPanel(props) {
    const classes = useStyles();
    const { action, value, index, modal } = props;  return (
        <div>
        {value === index && 
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.form}>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                {action.toUpperCase()}
            </Typography>
            {modal}
        </div>
        </Container>}
        </div>
    );
}

export default function LoginRegister({action, props}) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [signIn, setSignIn] = React.useState(() => { return (action === "login") ? 0 : 1});

    const handleOpen = () => {
        setOpen(true);
        setSignIn(() => {return (action === "login") ? 0 : 1});
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (event, newValue) => {
        setSignIn(newValue);
      };

    return(
        <>
            <Button className={action === "login" ? classes.login : classes.register} {...props} onClick={handleOpen}>{action}</Button>
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
                <Fade in={open}>
                    <div className={classes.paper}>
                        <IconButton className={classes.close} onClick={handleClose}>
                            <CloseIcon />
                        </IconButton>
                        <Tabs value={signIn} onChange={handleChange} variant="fullWidth" indicatorColor="primary">
                            <Tab label="login" />
                            <Tab label="register" />
                        </Tabs>
                        <TabPanel action="login" value={signIn} index={0} modal={<Login />} />
                        <TabPanel action="register" value={signIn} index={1} modal={<Register />} />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}