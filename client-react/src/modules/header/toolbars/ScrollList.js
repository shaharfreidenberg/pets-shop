import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 240,
    backgroundColor: theme.palette.background.paper,
    position: "absolute",
    overflow: "auto",
    zIndex: 1,
    maxHeight: 300,
    right: 0,
    top: "4.5rem",
    // border: "1px solid #000",
  },
  listSection: {
    backgroundColor: "inherit",
    border: "1px solid #000",
    height: "5rem",
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0,
  },
}));

export default function ScrollList() {
  const classes = useStyles();

  return (
    <List className={classes.root} subheader={<li />}>
      {[0, 1, 2, 3, 4].map((sectionId) => (
        <li key={`section-${sectionId}`} className={classes.listSection}>
          <ul className={classes.ul}>
            <ListSubheader>{`I'm sticky ${sectionId}`}</ListSubheader>
            {[0, 1, 2].map((item) => (
              <ListItem key={`item-${sectionId}-${item}`}>
                <ListItemText primary={`Item ${item}`} />
              </ListItem>
            ))}
          </ul>
        </li>
      ))}
    </List>
  );
}
