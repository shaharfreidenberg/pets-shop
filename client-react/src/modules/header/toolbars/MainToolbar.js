import {
  Badge,
  Container,
  IconButton,
  Toolbar,
  Popover,
  Button,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Context } from "../../../Context";
import SearchInput from "../../products/search/SearchInput";
import ShoppingList from "../../ShoppingList/ShoppingList";
import ScrollList from "./ScrollList";

const useStyles = makeStyles((theme) => ({
  logoImage: {
    width: 350,
    height: "auto",
    cursor: "pointer",
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
  },
  shoppingCart: {
    display: "grid",
    alignItems: "center",
  },
  badge: {
    backgroundColor: "#b0acd4",
    color: "white",
  },
  popover: {
    width: 320,
    padding: 20,
  },
}));

export default function MainToolbar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [context, setContext] = React.useContext(Context);
  const [showShoppingList, setShowShoppingList] = useState(true);
  const history = useHistory();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  React.useEffect(() => {
    if (context.user) {
      fetch(`http://localhost:8080/users/cartItems/${context.user.id}`)
        .then((response) => response.json())
        .then((data) => {
          const newContext = Object.assign({}, context);
          newContext.cartItems = data.cartItems;
          setContext(newContext);
        });
    }
  }, [context.user]);

  function handleLogoClick() {
    const newContext = Object.assign({}, context);
    newContext.search = "";
    newContext.petTypeId = undefined;
    newContext.productTypeId = undefined;

    setContext(newContext);
    history.push("/products");
  }

  return (
    <>
      <Toolbar>
        <Container className={classes.container}>
          <img
            className={classes.logoImage}
            src='PetsShop.png'
            alt='logo'
            onClick={handleLogoClick}
          />
          <SearchInput />

          <>
            <div className={classes.shoppingCart} onClick={handleClick}>
              <IconButton aria-label='cart'>
                <Badge
                  classes={{ badge: classes.badge }}
                  badgeContent={context.cartItems}
                >
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </div>
            <Popover
              classes={{ paper: classes.popover }}
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "center",
              }}
            >
              <ShoppingList onRedirect={handleClose} />
            </Popover>
          </>
        </Container>
      </Toolbar>
    </>
  );
}
