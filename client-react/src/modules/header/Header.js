import { AppBar, Divider } from "@material-ui/core";
import MainToolbar from "./toolbars/MainToolbar";
import MenuToolbar from "./toolbars/MenuToolbar";
import TopToolbar from "./toolbars/TopToolbar";

export default function Header() {
  return (
    <AppBar
      position="static"
      style={{ backgroundColor: "transparent", boxShadow: "none" }}
    >
      <TopToolbar />
      <MainToolbar />
      <Divider />
      <MenuToolbar />
      <Divider />
    </AppBar>
  );
}
