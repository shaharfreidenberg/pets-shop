import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}));

export default function Review(props) {
  const classes = useStyles();

  const user = JSON.parse(localStorage.getItem("user"));
  console.log(props.cart);

  return (
    <React.Fragment>
      <Typography variant='h6' gutterBottom>
        {"Order summary"}
      </Typography>
      <List disablePadding>
        {props.cart.map((product) => (
          <ListItem className={classes.listItem} key={product.name}>
            <ListItemText primary={product.name} secondary={product.amount} />
            <Typography variant='body2'>{product.price + " $"}</Typography>
          </ListItem>
        ))}
        <ListItem className={classes.listItem}>
          <ListItemText primary='Total' />
          <Typography variant='subtitle1' className={classes.total}>
            {props.totalPrice.toFixed(2) + " $"}
          </Typography>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant='h6' gutterBottom className={classes.title}>
            Shipping
          </Typography>
          <Typography gutterBottom>
            {user.firstName + " " + user.lastName}
          </Typography>
          <Typography gutterBottom>
            {props.shipping.address +
              ", " +
              props.shipping.city +
              ", " +
              props.shipping.phone}
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
