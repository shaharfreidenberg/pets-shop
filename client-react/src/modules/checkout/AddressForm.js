import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import React from "react";

export default function AddressForm(props) {
  const handleAddress = (e) => {
    props.onAddress(e);
  };
  const handlePhone = (e) => {
    props.onPhone(e);
  };
  const handleCity = (e) => {
    props.onCity(e);
  };

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Shipping address
      </Typography>
      <Grid container spacing={5}>
        <Grid item xs={12}>
          <TextField
            required
            id="address1"
            name="address1"
            label="Address line"
            fullWidth
            autoComplete="shipping address-line1"
            onInput={(e) => handleAddress(e.target.value)}
          />
        </Grid>
        <Grid item xs={16} sm={12}>
          <TextField
            required
            id="city"
            name="city"
            label="City"
            fullWidth
            autoComplete="shipping address-level2"
            onInput={(e) => handleCity(e.target.value)}
          />
        </Grid>
        <Grid item xs={16} sm={12}>
          <TextField
            required
            id="Phone"
            name="Phone"
            label="Phone"
            fullWidth
            autoComplete="Phone"
            onInput={(e) => handlePhone(e.target.value)}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
