import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Stepper from "@material-ui/core/Stepper";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React, { useEffect } from "react";
import AddressForm from "./AddressForm";
import Review from "./Review";
import { Context } from "../../Context";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
  closeButton: {
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));

const steps = ["Shipping address", "Review your order"];

export default function Checkout(props) {
  const classes = useStyles();
  const history = useHistory();
  const [activeStep, setActiveStep] = React.useState(0);
  const [address, setAddress] = React.useState("");
  const [city, setCity] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [context, setContext] = React.useContext(Context);
  const [cart, setcart] = React.useState([]);
  const { onClose } = props;
  const newContext = Object.assign({}, context);

  React.useEffect(() => {
    if (JSON.parse(localStorage.getItem("user")) === null)
      history.push("/products");
  }, []);

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <AddressForm
            onAddress={handleAddress}
            onCity={handleCity}
            onPhone={handlePhone}
          />
        );

      case 1:
        return (
          <Review
            cart={props.location.state.cart}
            totalPrice={props.location.state.totalPrice}
            shipping={{ address: address, city: city, phone: phone }}
          />
        );
      default:
        throw new Error("Unknown step");
    }
  }

  const removeCart = async () => {
    const userid = JSON.parse(localStorage.getItem("user")).id;

    const response = await fetch(
      "http://localhost:8080/users/shoppingCart/removeShoppingCart",
      {
        method: "PUT",
        body: JSON.stringify({
          userId: userid,
        }),
        headers: { "Content-Type": "application/json" },
      }
    );

    const json = await response.json();

    return json;
  };

  const handleNext = () => {
    if (address !== "" && city !== "" && phone !== "")
      setActiveStep(activeStep + 1);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const handleAddress = (address) => {
    setAddress(address);
  };
  const handleCity = (city) => {
    setCity(city);
  };
  const handlePhone = (phone) => {
    setPhone(phone);
  };

  const getCart = async (userid) => {
    const response = await fetch(
      `http://localhost:8080/users/ShoppingCart/getCart/${userid}`
    );

    const fetchedCart = await response.json();
    console.log("[fetchedCart]", fetchedCart);

    return fetchedCart;
  };

  const handlePlaceOrder = async () => {
    const userid = JSON.parse(localStorage.getItem("user")).id;

    const fetchedCart = await getCart(userid);

    const response = await fetch("http://localhost:8080/orders/create", {
      method: "POST",
      body: JSON.stringify({
        userId: userid,
        products: fetchedCart,
        address: address,
        city: city,
        phone: phone,
      }),
      headers: { "Content-Type": "application/json" },
    });

    await removeCart();
    handleNext();
    newContext.cartItems = 0;
    setContext(newContext);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar
        position='absolute'
        color='default'
        className={classes.appBar}
      ></AppBar>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component='h1' variant='h4' align='center'>
            Checkout
          </Typography>

          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant='h5' gutterBottom>
                  Thank you for your order.
                </Typography>
                <Typography variant='subtitle1'>
                  We have emailed your order confirmation, and will send you an
                  update when your order has shipped.
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <div className={classes.buttons}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} className={classes.button}>
                      Back
                    </Button>
                  )}
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={
                      activeStep === steps.length - 1
                        ? handlePlaceOrder
                        : handleNext
                    }
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? "Place order" : "Next"}
                  </Button>
                </div>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </main>
    </React.Fragment>
  );
}
