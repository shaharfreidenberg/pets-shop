import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { Paper, Card, CardContent } from '@material-ui/core';
import _ from 'lodash';
import Order from './Order';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  noItems: {
    padding: '32px',
    fontSize: '24px',
    textAlign: 'center'
  },
  accordion: {
    margin: '16px'
  }
}));

export default function PreviousOrders() {
  const [orders, setOrders] = React.useState([]);
  const classes = useStyles();

  React.useEffect(() => {
    fetch(`http://localhost:8080/orders/previous/${JSON.parse(localStorage.getItem("user")).id}`)
    .then((response) => response.json())
    .then((data) => setOrders(data))
}, []);

  return (
       <>
        { orders.length 
      ?  
      <TableContainer className={classes.accordion} component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>Order Id</TableCell>
              <TableCell>Order Date</TableCell>
              <TableCell>Products Amount</TableCell>
              <TableCell>Total Price</TableCell>
              <TableCell>Order Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { orders.map((order) => (
              <Order key={order._id} {...order} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      : <div className={classes.noItems}>No Orders To Show</div>
    }
       </>
  );
}