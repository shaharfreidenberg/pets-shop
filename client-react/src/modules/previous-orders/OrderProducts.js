import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import _ from 'lodash';

export default function OrderProcucts({ products }) {
    const productsGroupedById = Object.values(_.groupBy(products, '_id'));
    return (
        <Table size="small" aria-label="purchases">
        <TableHead>
          <TableRow>
            <TableCell>Product Id</TableCell>
            <TableCell>Product Name</TableCell>
            <TableCell>Price</TableCell>
            <TableCell>Amount</TableCell>
            <TableCell>Total Price</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        { productsGroupedById.map((group) =>
        <TableRow key={group[0]._id}>
          <TableCell component="th" scope="row">
            {group[0]._id}
          </TableCell>
          <TableCell>{group[0].name}</TableCell>
          <TableCell>{group[0].price}$</TableCell>
          <TableCell>{group.length}</TableCell>
          <TableCell>{group.map(p => p.price).reduce((sum, value) => sum + Number(value), 0)}$</TableCell>
        </TableRow>
        )}
        </TableBody>
      </Table>
    );
}