import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Collapse, IconButton, Typography, Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core';
import { KeyboardArrowDown, KeyboardArrowUp } from '@material-ui/icons';
import _ from 'lodash';
import OrderProcucts from './OrderProducts';
import { socketContext } from '../../Context';

const useRowStyles = makeStyles({
    root: {
      '& > *': {
        borderBottom: 'unset',
      },
    },
  });
  
export default function Order({ _id, date, products, price, isShipped }) {
    const [open, setOpen] = React.useState(false);
    const [shipped, setShipped] = React.useState(isShipped);
    const classes = useRowStyles();
    const totalPrice = products.map(p => p.price).reduce((sum, value) => sum + Number(value), 0);
    const socket = useContext(socketContext);

    React.useEffect(() => {
      socket.on('order status changed', (object) => {
        console.log(object.isShipped)
        if(_id === object.orderId) { setShipped(object.isShipped) }
      });
  }, []);

    return (
      <>
        <TableRow className={classes.root}>
          <TableCell>
            <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
              {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">{_id}</TableCell>
          <TableCell>{date}</TableCell>
          <TableCell>{products.length}</TableCell>
          <TableCell>{totalPrice}$</TableCell>
          <TableCell>{shipped ? 'shipped' : 'recieved'}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">Products</Typography>
                <OrderProcucts products={products} />
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </>
    );
  }