import { ListItemText } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import Checkout from "../checkout/Checkout";
import ShoppingListItem from "../ShoppingList/SoppingListItem";
import ShoppingCart from "../shopping-cart/ShoppingCart";
import { Link as RouterLink, useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    top: "5rem",
    zIndex: "1",
  },

  listItem: {
    borderBottom: "1px solid #000",
  },

  ListItemText: {
    color: "black",
  },
  Button: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  Divider: {
    width: "100%",
    maxWidth: 360,
  },
  CheckoutLayout: {
    position: "absolute",
    width: "100%",
    marginTop: theme.spacing(3),
  },

  backdrop: {
    position: "fixed",
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.5)",
    zIndex: "3",
    top: "0%",
    left: "0",
  },
}));

export default function ShoppingList(props) {
  const classes = useStyles();
  const history = useHistory();
  const [isCheckout, setIscheckout] = useState(false);
  const [cart, setcart] = React.useState([]);
  const [isCart, setIscart] = useState(false);
  let price = 0;

  React.useEffect(() => {
    if (JSON.parse(localStorage.getItem("user") !== null))
      fetch(
        `http://localhost:8080/users/ShoppingCart/${
          JSON.parse(localStorage.getItem("user")).id
        }`
      )
        .then((response) => response.json())
        .then((data) => setcart(data));
  }, []);

  if (cart.length === 0) return "Your cart is empty";
  else {
    cart.forEach((product) => (price += product.price * product.amount));
    return (
      <div>
        <List
          className={classes.root}
          style={{
            backgroundColor: "white",
          }}
        >
          {cart.map((product, index) => (
            <ShoppingListItem
              name={product.name}
              price={product.price}
              amount={product.amount}
              picture={`http://localhost:8080/img/${product.picture}`}
              key={index}
            />
          ))}
          <ListItem>
            <ListItemText
              className={classes.ListItemText}
              primary={"Total Price: " + price.toFixed(2) + " $"}
              style={{
                backgroundColor: "white",
              }}
            />
          </ListItem>
          <ListItem className={classes.Button}>
            <Button
              variant='contained'
              color='primary'
              disableElevation
              variant='contained'
              color='primary'
              onClick={() => {
                history.push("/shopping-cart");
                props.onRedirect();
              }}
            >
              VIEW CART
            </Button>
          </ListItem>
        </List>

        {isCheckout && (
          <div className={classes.backdrop}>
            <div style={{ marginTop: "0rem" }}>
              <Checkout style={classes.CheckoutLayout} />
            </div>
          </div>
        )}
        {isCart && (
          <div className={classes.backdrop}>
            <div style={{ marginTop: "0rem" }}>
              <ShoppingCart style={classes.CheckoutLayout} />
            </div>
          </div>
        )}
      </div>
    );
  }
}
