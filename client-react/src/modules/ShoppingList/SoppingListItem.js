import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: "36ch",
    backgroundColor: theme.palette.background.paper,
    color: "black",
  },
  inline: {
    display: "inline",
  },
  Divider: {
    width: "100%",
    maxWidth: 360,
  },
}));

export default function ShoppingListItem({
  name,
  price,
  amount,
  picture,
  key,
}) {
  const classes = useStyles();

  return (
    <div>
      <List className={classes.root} key={key}>
        <ListItem>
          <ListItemAvatar>
            <Avatar src={picture} />
          </ListItemAvatar>
          <ListItemText primary={name} secondary={price + " $"} />
          <ListItemText secondary={amount} />
        </ListItem>
        <Divider className={classes.Divider} />
      </List>
    </div>
  );
}
