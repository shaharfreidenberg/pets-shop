import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { AppBar, Container, Grid, List, ListItem, Toolbar, Typography, Link } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Info from '../header/toolbars/Info';
import PlaceIcon from '@material-ui/icons/Place';
import PhoneIcon from '@material-ui/icons/Phone';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PersonIcon from '@material-ui/icons/Person';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { Context } from '../../Context';

const useStyles = makeStyles((theme) => ({
    footer: {
        top: 'auto',
        bottom: 0,
        backgroundColor: "rgba(215,215,221,0.28)",
    },
    footerTitle: {
        textTransform: "uppercase",
        color : "black",
        paddingBottom: 15,
    },
    footerText: {
        color: "#727272",
    },
    footerItem: {
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 30,
        paddingBottom: 30,
    },
    footerLink: {
        color: "#727272",
        '&:hover': {
            color: "black"
        },
    },
}));


export default function Footer() {
    const classes = useStyles();
    const [petTypes, setPetTypes] = React.useState([]);
    const [context, setContext] = React.useContext(Context);

    React.useEffect(() => {        
        fetch('http://localhost:8080/petTypes')
        .then((response) => response.json())
        .then((data) => setPetTypes(data));
    }, []);

    function handleCategoryClick(petTypeId) {
        const newContext = Object.assign({}, context);
        newContext.petTypeId = petTypeId;
        newContext.search = '';
        newContext.productTypeId = undefined;
        setContext(newContext);
    }

    return (
        <AppBar component="footer" position="static" className={classes.footer}>
            <Toolbar>
                <Container>
                    <Grid container direction="row" justify="space-between">
                        <Grid item xs className={classes.footerItem}>
                            <Typography className={classes.footerTitle} variant="h6">about us</Typography>
                            <List disablePadding>
                                <ListItem disableGutters>
                                    <Typography className={classes.footerText} variant="body2" gutterBottom>
                                        We have everything you need for your pet at amazing prices, every day.<br/>
                                        At Pets Shop, we strive to deliver the best products with the best service – 
                                        and we want to become even better. <br/>
                                        The only thing more important to us than a happy customer is a happy pet.
                                    </Typography>
                                </ListItem>
                            </List>
                        </Grid>
                        <Grid item xs className={classes.footerItem}>
                            <Typography className={classes.footerTitle} variant="h6">categories</Typography>
                            <List disablePadding>
                                {petTypes.map((petType) => (
                                    <ListItem disableGutters>
                                        <Link key={petType._id} onClick={() => handleCategoryClick(petType._id)} className={classes.footerLink} component={RouterLink} to={`/products`} underline="none">
                                            {petType.name}
                                        </Link>
                                    </ListItem>
                                ))}
                            </List>
                        </Grid>
                        <Grid item xs className={classes.footerItem}>
                            <Typography className={classes.footerTitle} variant="h6">contact us</Typography>
                            <Typography className={classes.footerText} variant="body2" gutterBottom>
                                <List disablePadding>
                                    <ListItem disableGutters>
                                        <Info icon={<PlaceIcon fontSize="small" />} text="Ben Yehuda 5, Tel Aviv"/>
                                    </ListItem>
                                    <ListItem disableGutters>
                                        <Info icon={<PhoneIcon fontSize="small" />} text="03-4060809"/>
                                    </ListItem>
                                    <ListItem disableGutters>
                                        <Info icon={<WhatsAppIcon fontSize="small" />} text="050-3200000"/>
                                    </ListItem>
                                    <ListItem disableGutters>
                                        <Info icon={<MailOutlineIcon fontSize="small" />} text="info@pets-store.com"/>
                                    </ListItem>
                                </List>
                            </Typography>
                        </Grid>
                        <Grid item xs className={classes.footerItem}>
                            <Typography className={classes.footerTitle} variant="h6">services</Typography>
                            <List disablePadding>
                                <ListItem disableGutters>
                                    <Link className={classes.footerLink} component={RouterLink} to="/my-account" underline="none">
                                        <Info icon={<PersonIcon fontSize="small" />} text="My Account"/>
                                    </Link>
                                </ListItem>
                                <ListItem disableGutters>
                                    <Link className={classes.footerLink} component={RouterLink} to="/#" underline="none">
                                        <Info icon={<ShoppingCartIcon fontSize="small" />} text="Cart"/>
                                    </Link>
                                </ListItem>
                            </List>
                        </Grid>
                    </Grid>
                </Container>
            </Toolbar>
        </AppBar>
    );
}