import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, TextField } from '@material-ui/core';
import { Context } from '../../Context';
import MuiAlert from '@material-ui/lab/Alert';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    argin: theme.spacing(3, 0, 2),
    backgroundColor: "#BCB9DD",
  },
}));

// Used for update too
export default function Register() {
  const classes = useStyles();
  const [context, setContext] = React.useContext(Context);
  const [fName, setfName] = React.useState((context.user) ? context.user.firstName : '');
  const [lName, setlName] = React.useState(((context.user) ? context.user.lastName : ''));
  const [email, setEmail] = React.useState(((context.user) ? context.user.email : ''));
  const [password, setPassword] = React.useState('');
  const [emailError, setEmailError] = React.useState('');
  const [passwordError, setPasswordError] = React.useState('');
  const [alert, setAlert] = React.useState('');
  const [severity, setSeverity] = React.useState('');
  let registerUrl = 'http://localhost:8080/users/register';
  let updateUrl = 'http://localhost:8080/users/'

  const isFormValid = () => {
    setEmailError("");
    setPasswordError("");
    let isValid = true;
    let regex =  /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

    if (!regex.test(email)) {
      setEmailError("Please enter a valid email");
      isValid = false;
    }

    if (password.length < 8){
      setPasswordError("Password must contain at least 8 characters");
      isValid = false;
    }

    return isValid;
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setAlert('');

    if (isFormValid()) {
      let url = "";
      let method = "";
      
      if (context.user) {
        url = updateUrl;
        method = 'PUT';
      }
      else {
        url = registerUrl;
        method = 'POST';
      }

      const requestOptions = {
        method: method,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 
          id: (context.user) ? context.user.id : undefined,
          firstName: fName,
          lastName: lName,
          email: email,
          password: password
        })
      };

      fetch(url, requestOptions).then(response => {
        let json = response.json();
        if (response.ok) return json;
        else return json.then(err => {throw err});
      })
      .then(data => {
        const newContext = Object.assign({}, context);
        newContext.user = data;
        setContext(newContext);
        localStorage.setItem("user", JSON.stringify(data));
        setAlert("Done !");
        setSeverity("success");
      })
      .catch(data => {
        if (data.errors) {
          data.errors.map(error => {
            if (error.param === "id"){
              setAlert(error.msg);
              setSeverity("error");
            }
            else {
              (error.param === "email") ? setEmailError(error.msg) : setEmailError("");
              (error.param === "password") ? setPasswordError(error.msg) : setPasswordError("");
            }
          });
        }
        else {
          setAlert(data.message);
          setSeverity("error");
        }
      });
    }
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
        <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
            <TextField autoComplete="fname" name="firstName" variant="outlined" required fullWidth id="firstName" 
              label="First Name" value={fName} onInput={e=>setfName(e.target.value)} autoFocus/>
        </Grid>
        <Grid item xs={12} sm={6}>
            <TextField autoComplete="lname" name="lastName" variant="outlined" required fullWidth id="lastName" 
            label="Last Name" value={lName} onInput={e=>setlName(e.target.value)}/>
        </Grid>
        <Grid item xs={12}>
            <TextField autoComplete="email" name="email" variant="outlined" required fullWidth id="email" 
            label="Email Address" value={email} onInput={e=>setEmail(e.target.value)} error={emailError !== ""} helperText={emailError}/>
        </Grid>
        <Grid item xs={12}>
            <TextField autoComplete="current-password" name="password" variant="outlined" required fullWidth type="password" 
            label="Password" value={password} onInput={e=>setPassword(e.target.value)} error={passwordError !== ""} helperText={passwordError}/>
        </Grid>
        </Grid>
        <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
        {(context.user) ? "Update" : "Register"}
        </Button>
        {alert && <Alert severity={severity} variant="filled">{alert}</Alert>}
    </form>
  );
}