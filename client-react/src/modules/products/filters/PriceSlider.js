import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  typography: {
      color: 'black',
      fontWeight: '500',
      marginBottom: '2.2em',
      textAlign: 'left'
  },
  slider: {
      color: '#b0acd4'
  }
});

export default function RangeSlider(props) {
  console.log(props);
  const classes = useStyles();
  const [value, setValue] = React.useState([props.minValue, props.maxValue]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeCommitted = (event, newValue) => {
    props.onFilter(newValue);
  };

  return (
    <div className={classes.root}>
      <Typography id="range-slider" gutterBottom className={classes.typography}> 
        Filter By Price Range
      </Typography>
      <Slider
        classes={{ root: classes.slider }}
        value={value}
        onChange={handleChange}
        onChangeCommitted={handleChangeCommitted}
        valueLabelDisplay="on"
        aria-labelledby="range-slider"
        min={props.minValue}
        max={props.maxValue}
      />
    </div>
  );
}