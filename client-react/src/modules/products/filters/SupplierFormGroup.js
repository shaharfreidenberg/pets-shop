import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    marginTop: '12px'
  },
  formLabel: {
    fontWeight: '500',
    textAlign: 'left'
  }
}));

export default function Supplier(props) {
  const classes = useStyles();

  const handleChange = (event) => {
      props.onCheck(event.target.name, event.target.checked);
  };

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend" className={classes.formLabel} style ={{ color: 'black' }}>Filter By Supplier</FormLabel>
        <FormGroup>
            {Object.keys(props.options).map(option =>
                <FormControlLabel key={option}
                control={<Checkbox style ={{ color: '#b0acd4' }} 
                checked={props.options[option]} onChange={handleChange} name={option} />}
                label={option}
              />
                )}
        </FormGroup>
      </FormControl>
    </div>
  );
}