import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Context } from '../../../Context'
import { useHistory } from "react-router-dom";

export default function SearchInput() {
  const [context, setContext] = React.useContext(Context);
  const [products, setProducts] = React.useState([]);
  const history = useHistory();

  React.useEffect(() => {
    fetch('http://localhost:8080/products')
    .then((response) => response.json())
    .then((data) => setProducts(data));
}, []);

  function handleChange(event, value) {
    const newContext = Object.assign({}, context);
    newContext.search = value;
    newContext.petTypeId = undefined;
    newContext.productTypeId = undefined;
    setContext(newContext);
    history.push('/products');
  }

  return (
    <div style={{ width: 400 }}>
      <Autocomplete
        id="products-search-input"
        onChange={handleChange}
        freeSolo
        options={products.map((option) => option.name)}
        renderInput={(params) => (
          <TextField {...params} label="search product..." margin="normal" variant="outlined" />
        )}
      />
    </div>
  );
}