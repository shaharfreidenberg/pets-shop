import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Products from './list/Products';
import { Context } from '../../Context';
import PriceSlider from './filters/PriceSlider';
import SupplierFormGroup from './filters/SupplierFormGroup';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: '12px'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }, 
  noItems: {
    padding: '32px',
    fontSize: '24px',
    textAlign: 'center'
  }
}));

export default function ProductsWithFilter() {
  const classes = useStyles();
  const [products, setProducts] = React.useState([]);
  const [query, setQuery] = React.useState('');
  const [priceRange, setPriceRange] = React.useState({min: 0, max: 500});
  const [suppliers, setSuppliers] = React.useState({});
  const [context, setContext] = React.useContext(Context);
  const baseUrl = 'http://localhost:8080/products';

  React.useEffect(() => {
    if(context.search) { setQuery(`${baseUrl}/search/${context.search}`) }
    else if(context.productTypeId) { setQuery(`${baseUrl}/types/${context.productTypeId}`) }
    else if(context.petTypeId) { setQuery(`${baseUrl}/pets/${context.petTypeId}`) }
    else { setQuery(baseUrl)};
  }, [context.petTypeId, context.productTypeId, context.search]);

  React.useEffect(() => {
    if(query) {
      fetch(query)
      .then((response) => response.json())
      .then((data) => setProducts(data));
    }
}, [query]);

React.useEffect(() => {
    var obj = new Object();
    _.uniq(_.map(products, 'supplier')).map(supplier => { obj[supplier] = true });;
    setSuppliers(obj);
}, [products]);


function getFilteredProducts() {
    // get the true keys
    const filteredSuppliers = _.keys(_.pickBy(suppliers));
    // price filter
    var filteredProducts = products
    .filter(product => Number(product.price) >= Number(priceRange.min) && Number(product.price <= Number(priceRange.max)))
    // supplier filter
    .filter(product => filteredSuppliers.includes(product.supplier));

    return filteredProducts;
}

function handlePriceFilter(value) {
    setPriceRange({min: value[0], max: value[1]});
}

function handleSupplierFilter(name, checked) {
    setSuppliers({ ...suppliers, [name]: checked });
}

  return (
    <div className={classes.root}>
      <Grid container spacing={0}>
        <Grid item xs={2}>
          <div className={classes.paper}>
            <PriceSlider onFilter={handlePriceFilter} maxValue={1000} minValue={0} />
            { Object.keys(suppliers).length 
              ? <SupplierFormGroup options={suppliers} onCheck={handleSupplierFilter} /> 
              : <></> }
          </div>
        </Grid>
        <Grid item xs={10}>
        { getFilteredProducts().length
          ? <Products products={getFilteredProducts()} />
          : <div className={classes.noItems}>No Products To Show</div>
        }
        </Grid>
      </Grid>
    </div>
  );
}