import React from 'react';
import Divider from '@material-ui/core/Divider';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    whiteSpace: 'pre-line',
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const useStyles = makeStyles((theme) => ({
    img: {
      height: 280,
      width: 400,
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto'
    },
    subtitle: {
        fontWeight: 'bold'
    }
  }));

export default function ProductDialog(props) {
    const classes = useStyles();
  return (
    <>
      <Dialog onClose={() => props.handleClose()} aria-labelledby="customized-dialog-title" open={props.open}>
        <DialogTitle id="customized-dialog-title" onClose={() => props.handleClose()}>
          {props.name}
        </DialogTitle>
        <DialogContent dividers>
        <Typography gutterBottom>
              <span className={classes.subtitle}>Price:</span> {props.price}
          </Typography>
          <Typography gutterBottom>
              <span className={classes.subtitle}>Supplier:</span> {props.supplier}
          </Typography>
          <Typography gutterBottom>
              <div className={classes.subtitle}>More Details:</div> {props.description}
          </Typography>
          <Divider />
          <img className={classes.img} src={`http://localhost:8080/img/${props.picture}`}></img>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.handleClose()}>
            No Thanks
          </Button>
          <Button onClick={() => props.handleClose(props._id)}>
            Add To Cart
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}