import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import ProductDialog from './ProductDialog';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '100%',
    height: '100%',
  },
  icon: {
    color: 'white',
  },
  bar: {
    height: '68px',
  },
  barSubtitle: {
    lineHeight: '18px'
  },
  tile: {
    cursor: 'pointer'
  }
}));

export default function ProductsList({ products, onCartIconClick, onProductClick }) {
  const classes = useStyles();

  function handleIconClick(e, itemId) {
    e.stopPropagation();
    onCartIconClick(itemId);
  }
  
  return (
    <div className={classes.root}>
      <GridList cellHeight={240} cols={4} className={classes.gridList}>
        { products.map((tile) => (
          <GridListTile key={tile._id} onClick={() => onProductClick(tile)} className={classes.tile}>
            <img src={`http://localhost:8080/img/${tile.picture}`} alt={tile.name} />
            <GridListTileBar className={classes.bar}
              title={tile.name}
              subtitle={
                <div className={classes.barSubtitle}>
                  <div>Price: {tile.price}</div>
                  <div>Supplier: {tile.supplier}</div>
                </div>
              }
              actionIcon={
                <IconButton disabled={!JSON.parse(localStorage.getItem("user"))} aria-label='add to cart' 
                  className={classes.icon} onClick={(e) => handleIconClick(e, tile._id)}>
                  <AddShoppingCartIcon />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  ); 
}