import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import ProductDialog from './ProductDialog';
import ProductsList from './ProductsList';
import { Context } from '../../../Context';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Products(props) {
  const [snackbarState, setSnackbarState] = React.useState({open: false, status: 'success', message: ''});
  const [dialogState, setDialogState] = React.useState({dialogOpen: false, item: {}});
  const [context, setContext] = React.useContext(Context);

  function handleItemAdded(itemId) {
    if(JSON.parse(localStorage.getItem("user"))) {
      fetch('http://localhost:8080/users/shoppingCart', {
        method:'PUT',
        body: JSON.stringify({ productId: itemId, userId: JSON.parse(localStorage.getItem("user")).id }),
        headers: {'Content-Type': 'application/json'}
      })
      .then((response => {
        if (response.ok) {
          setSnackbarState({open: true, status: 'success', message: 'item added to your cart'});
          const newContext = Object.assign({}, context);
          newContext.cartItems = context.cartItems + 1;
          setContext(newContext);
        }
        else setSnackbarState({open: true, status: 'error', message: 'error occured'});
      }));
    } else {
      setSnackbarState({open: true, status: 'error', message: 'you must login before adding items to your cart'})
    }
  }

  function handleSnackbarClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setSnackbarState({open: false, status: snackbarState.status, message: snackbarState.message});
  };

  function handleTileClick(item) {
    setDialogState({dialogOpen: true, item: item});
  };

  function handleDialogClose(itemId) {
    if(itemId) { handleItemAdded(itemId); }
    setDialogState({dialogOpen: false, item: {}});
  }
  
  return (
      <>
        <ProductsList onCartIconClick={handleItemAdded} onProductClick={handleTileClick} products={props.products} />
        <Snackbar open={snackbarState.open} autoHideDuration={3000} onClose={handleSnackbarClose}>
            <Alert onClose={handleSnackbarClose} severity={snackbarState.status}>
              {snackbarState.message}
            </Alert>
        </Snackbar>
        <ProductDialog handleClose={handleDialogClose} {...dialogState.item} open={dialogState.dialogOpen} />
    </>
  ); 
}