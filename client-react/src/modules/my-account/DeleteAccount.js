import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Context } from '../../Context';
import Alert from '@material-ui/lab/Alert';
import DeleteIcon from '@material-ui/icons/Delete';

export default function DeleteAccount() {
    const [open, setOpen] = React.useState(false);
    const [context, setContext] = React.useContext(Context);
    const [alert, setAlert] = React.useState('');
    let history = useHistory();
    let deleteurl = 'http://localhost:8080/users/';

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const handleDelete = () => {
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
              id: context.user.id
            })
          };
      
          fetch(deleteurl, requestOptions).then(response => {
            let json = response.json();
            if (response.ok) return json;
            else return json.then(err => {throw err});
          })
          .then(data => {
            const newContext = Object.assign({}, context);
            newContext.user = undefined;
            setContext(newContext);
            localStorage.removeItem("user");
            history.replace("/products");
          })
          .catch(data => {
            setOpen(false);
            if (data.errors) data.errors.map(error => setAlert(error.msg));
            else setAlert(data.message);
          });
      };

    return(
        <>
        <Button variant="contained" color="primary" onClick={handleClickOpen} startIcon={<DeleteIcon />}>
            Delete
        </Button>
        {alert && <Alert style={{marginTop: 10}} severity="error" variant="filled">{alert}</Alert>}
        <Dialog open={open} onClose={handleClose} fullWidth="true"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
            <DialogTitle id="alert-dialog-title">Delete Account</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Are you sure you want to delete your account? <br/>
                    This cannnot be undone!
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>
                    Cancel
                </Button>
                <Button onClick={handleDelete} color="primary" autoFocus variant="contained">
                    Delete
                </Button>
            </DialogActions>
        </Dialog>
      </>
    );
}