import React from 'react';
import { Box, Button, Divider, Tab, Tabs, Typography } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { Context } from '../../Context';
import PreviousOrders from '../previous-orders/PreviousOrders';
import Register from '../register/Register';
import DeleteAccount from './DeleteAccount';

const useStyles = makeStyles((theme) => ({
    title: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    subtitle :{
        marginBottom: theme.spacing(2),
    },
    tabs: {
        display: "flex",
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    noUser: {
        display: "flex",
        justifyContent: "center",
    },
  }));

function TabPanel(props) {
    const { children, value, index, ...other } = props;  
    return(
        <div {...other} style={{marginLeft: 25}}>
            {value === index && 
            <Box p={1}>{children}</Box>}
        </div>
    );
}

export default function MyAccount() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [context, setContext] = React.useContext(Context);
    
    const handleChange = (event, newValue) => {
        setValue(newValue);
      };

    return(
        <>
        {!context.user && 
            <div className={classes.noUser}>
                <Typography className={classes.title} variant="h4">
                    In order to manage your account you need to login or register
                </Typography>
            </div>}
            {context.user && 
            <>
            <Typography className={classes.title} variant="h4">
                Manage your account
            </Typography>
            <Divider />
            <div className={classes.tabs}>
                <Tabs orientation="vertical" value={value} onChange={handleChange} indicatorColor="primary">
                    <Tab label="Change" />
                    <Tab label="Delete" />
                </Tabs>
                <TabPanel value={value} index={0}>
                    <Typography className={classes.subtitle} variant="h6">
                        Change your account settings:
                    </Typography>
                   <Register /> 
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Typography className={classes.subtitle} variant="h6">
                        Delete your account:
                    </Typography>
                    <Typography className={classes.subtitle} variant="body1">
                        <b>BE AWARE!</b> You will lose access to your orders and you won't be able to login again
                    </Typography>
                    <DeleteAccount />
                </TabPanel>
            </div>
            </>}
        </>
    );
}