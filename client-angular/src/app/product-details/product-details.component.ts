import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { ToastrService } from 'ngx-toastr';
import { DialogComponent } from '../dialog/dialog.component';
import { PetType } from '../products/models/petType';
import { Product } from '../products/models/product';
import { ProductType } from '../products/models/productType';
import { PetTypesService } from '../products/services/pet-types.service';
import { ProductTypesService } from '../products/services/product-types.service';
import { ProductsService } from '../products/services/products.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  @Input() product: Product;
  @Output() refreshList = new EventEmitter();
  productPicture: any;
  productImgUrl: any;
  newImage: File = null;
  isEdit: boolean = false;
  productTypes: ProductType[];
  petTypes: PetType[];
  formGroup: FormGroup;

  constructor(private productService: ProductsService, 
              private productTypeService: ProductTypesService,
              private petTypeService: PetTypesService,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.productService.getProductPicture(this.product._id).subscribe(data => {
      this.productImgUrl = data;
      this.loadImage(data);
    });

    this.petTypeService.getAllPetTypes().subscribe(data => this.petTypes = data);
    this.productTypeService.getTypeByPet(this.product.petType._id).subscribe(data => this.productTypes = data);
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      'id': [{value: this.product._id, disabled: true}, Validators.required],
      'name': [{value: this.product.name, disabled: !this.isEdit}, Validators.required],
      'petType': [{value: this.product.petType._id, disabled: !this.isEdit}, Validators.required],
      'productType': [{value: this.product.productType._id, disabled: !this.isEdit}, Validators.required],
      'price': [{value: this.product.price, disabled: !this.isEdit}, [Validators.required, 
        Validators.pattern("^([1-9]+\\d*([.]\\d+)?)$|^(-?0[.]\\d*[1-9]+)$|^0$"), 
        Validators.min(0)]],
      'supplier': [{value: this.product.supplier,  disabled: !this.isEdit}, Validators.required],
      'description': [{value: this.product.description, disabled: !this.isEdit}, Validators.required],
    });
  }

  onEditClick() {
    if (this.isEdit){
      this.newImage = null;
      this.loadImage(this.productImgUrl);
      this.productTypeService.getTypeByPet(this.product.petType._id).subscribe(data => this.productTypes = data);
      this.formGroup.reset({id: this.product._id,
                            name: this.product.name,
                            petType: this.product.petType._id,
                            productType: this.product.productType._id,
                            price: this.product.price,
                            supplier: this.product.supplier,
                            description: this.product.description});
      this.formGroup.disable();
    }
    else {
      this.formGroup.enable();
    }

    this.isEdit = !this.isEdit;
    this.formGroup.get('id').disable();
  }

  onSubmit(formData) {
    if(this.formGroup.valid) {
      this.productService.updateProduct({
        _id: this.product._id, 
        name: formData.name,
        petType: formData.petType,
        productType: formData.productType,
        description: formData.description,
        price: formData.price,
        supplier: formData.supplier,
        picture: this.product.picture
      }).subscribe(data => {
        if(this.newImage) {
          this.productService.uploadImage(this.product._id, this.newImage, false).subscribe(data => {
            this.toastr.success(data.message, "Succeess");
            this.refreshList.emit(null);
          },
            err => this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}}))
        }
        else {
          this.toastr.success(data.message, "Succeess");
          this.refreshList.emit(null);
        }
      },
      err => {
        this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}});
      });
    }
  }

  onDeleteClick() {
    const dialogRef = this.dialog.open(DialogComponent, {data: {
      title: "Warning", 
      message: "Are you sure tou want to delete product " + this.product._id + ": " + this.product.name,
      isDelete: true}});

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productService.delete(this.product._id).subscribe(data => {
          this.toastr.success(data.message, "Succeess");
          this.refreshList.emit(null);
        },
        err => {
          this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}});
        });
      }
    });
  }

  onPetChange(event: MatSelectChange) {
    this.productTypeService.getTypeByPet(event.value).subscribe(data => this.productTypes = data);
    this.formGroup.get('productType').setValue((event.value === this.product.petType._id) ? this.product.productType._id : '');
  }

  previewImage(event) {
    this.newImage = <File>event.target.files[0];
    this.loadImage(event.target.files[0]);
  }

  loadImage(path) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.productPicture = reader.result;
    }, false);

    if (path) {
      reader.readAsDataURL(path);
    }
  }
}
