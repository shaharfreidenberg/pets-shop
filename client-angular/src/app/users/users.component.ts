import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { User } from './user';
import { UsersService } from './users.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements AfterViewInit  {
  error: string[] = [];
  isLoadingResults: boolean = true;        
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'isAdmin', 'action'];
  dataSource = new MatTableDataSource<User>();

  constructor(private usersService: UsersService) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  onClick(id: string, position: number) {
    this.usersService.changePermissions(id).subscribe(data => this.loadData(),
    err => {
      this.error[position] = err.error.message;
    })
  }

  loadData() {
    this.error = [];
    this.isLoadingResults = true;
    this.usersService.getAllUsers().subscribe(data => {
      this.dataSource.data = data;
      timer(500).subscribe(x => { this.isLoadingResults = false });
    });
  }

  applyFilter(event: Event) {
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return data.firstName.toLowerCase().includes(filter) || 
             data.lastName.toLowerCase().includes(filter) || 
             data.email.toLowerCase().includes(filter);
    };
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}