import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getAllUsers() : Observable<User[]>  {
    return this.http.get<User[]>(environment.allUsersUrl);
  }

  changePermissions(id: string) {
    return this.http.put(environment.changePermissionsUrl, { id: id });
  }
}
