import { CanActivate, Router } from '@angular/router';
import { AuthService } from './login/auth.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router) {}
    
    canActivate() : Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.isLoggedIn().pipe(map(logged => {
            if (logged) { return true}
            this.router.navigate(['/login']);
            return false;
        }))
    }
}