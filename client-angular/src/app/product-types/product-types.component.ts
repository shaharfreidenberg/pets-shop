import { animate, style, transition, trigger } from '@angular/animations';
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { timer } from 'rxjs';
import { DialogComponent } from '../dialog/dialog.component';
import { PetType } from '../products/models/petType';
import { ProductType } from '../products/models/productType';
import { PetTypesService } from '../products/services/pet-types.service';
import { ProductTypesService } from '../products/services/product-types.service';

@Component({
  selector: 'app-product-types',
  templateUrl: './product-types.component.html',
  styleUrls: ['./product-types.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ]),
    trigger('slideLeftRight', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-out', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ProductTypesComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'petType', 'edit', 'delete'];
  dataSource = new MatTableDataSource<ProductType>();
  visible: boolean = false;
  newName = new FormControl('', [Validators.required]);
  newPetType = new FormControl('', [Validators.required]);
  petTypes: PetType[];
  isEdit:boolean = false;
  formGroup: FormGroup;

  constructor(private productTypeService: ProductTypesService,
              private petTypeService: PetTypesService,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private formBuilder: FormBuilder) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.petTypeService.getAllPetTypes().subscribe(data => this.petTypes = data);
    this.loadData();
    this.createForm();
  }

  loadData() {
    this.productTypeService.getAll().subscribe(data => this.dataSource.data = data);
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      'id': [{value: '', disabled: true}],
      'name': ['', Validators.required],
      'petType': ['', Validators.required]
    });
  }

  onEditClick(productType: ProductType) {
    this.isEdit = false;
    timer(500).subscribe(x => { 
      this.formGroup.setValue({ 
        id: productType._id,
        name: productType.name,
        petType: productType.petType._id
      });
      this.formGroup.get('id').disable();
      this.isEdit = true 
    });
  }

  onCancelEditClick() {
    this.isEdit = false;
  }

  onSubmit(formData) {
    if(this.formGroup.valid) {
      this.productTypeService.update({
        _id: this.formGroup.get('id').value, 
        name: formData.name,
        petType: formData.petType
      }).subscribe(data => {
          this.toastr.success(data.message, "Succeess");
          this.loadData();
          this.isEdit = false;
      },
      err => {
        this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}});
      });
    }
  }

  onDeleteClick(id: string) {
    const dialogRef = this.dialog.open(DialogComponent, {data: {
      title: "Warning", 
      message: `Are you sure you want to delete product type ${id}? 
                Make sure that there are no products that are related to this product type, otherwise they will be deleted as well.`,
      isDelete: true}});

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productTypeService.delete(id).subscribe(data => {
          this.toastr.success(data.message, "Succeess");
          this.loadData();
        },
        err => {
          this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}});
        });
      }
    });
  }

  addProductType() {
    this.visible = true;

    if(this.newName.valid && this.newPetType.valid && this.visible) {
      this.productTypeService.create(this.newName.value, this.newPetType.value).subscribe(data => {
        this.toastr.success(data.message, "Succeess");
        this.loadData();
        this.onCancelClick();
      },
      err => this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}}));
    }
  }

  onCancelClick() {
    this.visible = false;
    this.newName.setValue('');
    this.newName.markAsUntouched();
    this.newPetType.setValue('');
    this.newPetType.markAsUntouched();
  }

  onRefresh() {
    this.loadData();
  }
}
