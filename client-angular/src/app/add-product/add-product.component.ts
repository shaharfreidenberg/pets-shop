import { Component, OnInit } from '@angular/core';
import {formatNumber, Location} from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductType } from '../products/models/productType';
import { PetType } from '../products/models/petType';
import { ProductTypesService } from '../products/services/product-types.service';
import { PetTypesService } from '../products/services/pet-types.service';
import { MatSelectChange } from '@angular/material/select';
import { ProductsService } from '../products/services/products.service';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  productPicture: any;
  formGroup: FormGroup;
  newImage: File = null;
  productTypes: ProductType[];
  petTypes: PetType[];
  imageError: boolean = false;

  constructor(private _location: Location,
              private formBuilder: FormBuilder,
              private productTypeService: ProductTypesService,
              private petTypeService: PetTypesService,
              private productService: ProductsService,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.productPicture = '../assets/add-image.png';
    this.petTypeService.getAllPetTypes().subscribe(data => this.petTypes = data);
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      'name': ['', Validators.required],
      'petType': ['', Validators.required],
      'productType': ['', Validators.required],
      'price': ['', [Validators.required, 
                     Validators.pattern("^([1-9]+\\d*([.]\\d+)?)$|^(-?0[.]\\d*[1-9]+)$|^0$"), 
                     Validators.min(0)]],
      'supplier': ['', Validators.required],
      'description': ['', Validators.required],
    });
  }

  onBackClick(){
    this._location.back();
  }

  onSubmit(formData) {
    (this.newImage != null) ? this.imageError = false : this.imageError = true;
    if(this.formGroup.valid && !this.imageError) {
      this.productService.create({
        _id: '',
        name: formData.name,
        petType: formData.petType,
        productType: formData.productType,
        price: formData.price,
        supplier: formData.supplier,
        description: formData.description,
        picture: ''
      }).subscribe(data => {
        this.productService.uploadImage(data._id, this.newImage, true).subscribe(data => {
          this.toastr.success(data.message, "Succeess");
          this.onBackClick();
        },
        err => this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}}))
      },
      err => this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}}));
    }
  }

  onPetChange(event: MatSelectChange) {
    this.productTypeService.getTypeByPet(event.value).subscribe(data => this.productTypes = data);
  }

  previewImage(event) {
    this.newImage = <File>event.target.files[0];
    this.loadImage(event.target.files[0]);
  }

  loadImage(path) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.productPicture = reader.result;
    }, false);

    if (path) {
      reader.readAsDataURL(path);
    }
  }
}
