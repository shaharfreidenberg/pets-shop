import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MonthlyIncomesDirective } from './statistics/monthly-incomes.directive';
import { StatisticsComponent } from './statistics/statistics.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { ToastrModule } from 'ngx-toastr';
import { OrderNotificationComponent } from './order-notification/order-notification.component';
import { LoginComponent } from './login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './auth.guard';
import { GuestGuard } from './guest.guard';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { UsersComponent } from './users/users.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { OrdersComponent } from './orders/orders.component';
import { ProductsComponent } from './products/products.component';
import { ProductDetailsComponent } from './product-details/product-details.component'; 
import { MatDialogModule } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component'; 
import { MatGridListModule } from '@angular/material/grid-list';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductsFilterComponent } from './products-filter/products-filter.component'; 
import { FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { PetTypesComponent } from './pet-types/pet-types.component';
import { ProductTypesComponent } from './product-types/product-types.component';
import { OrderProductsComponent } from './orders/order-products/order-products.component';
import { CountersComponent } from './statistics/counters/counters.component';
import { CheckboxComponent } from './orders/checkbox/checkbox.component';

const config: SocketIoConfig = { url: 'http://localhost:8081', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    MonthlyIncomesDirective,
    StatisticsComponent,
    OrderNotificationComponent,
    LoginComponent,
    UsersComponent,
    OrdersComponent,
    ProductsComponent,
    ProductDetailsComponent,
    DialogComponent,
    AddProductComponent,
    ProductsFilterComponent,
    PetTypesComponent,
    ProductTypesComponent,
    OrderProductsComponent,
    CountersComponent,
    CheckboxComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    SocketIoModule.forRoot(config),
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatDialogModule,
    MatGridListModule,
    FormsModule,
    MatTabsModule
  ],
  providers: [HttpClientModule, AuthGuard, GuestGuard],
  bootstrap: [AppComponent]
})

export class AppModule { }
