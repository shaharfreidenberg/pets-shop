import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Order } from './orders/order';

@Injectable({
    providedIn: 'root'
})

export class RealtimeService {
    connectedUsersCount = this.socket.fromEvent<number>('connected users count');
    newRegisteredUser = this.socket.fromEvent('new registered user');
    newProduct = this.socket.fromEvent('new product');
    newOrder = this.socket.fromEvent<string>('new order');
    constructor(private socket: Socket) { }
}