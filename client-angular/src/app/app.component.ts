import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from './login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isLoggedIn: boolean;
  routeUrl: string;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authService.isLoggedIn().subscribe(data => {
      this.isLoggedIn = data;
    });

    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        this.routeUrl = this.router.url;
      }
    })
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  menu = [{name: "Dashboard", icon: "dashboard", route: "/statistics"}, 
          {name: "Products", icon: "pets", route: "/products"},
          {name: "Orders", icon: "assignment_turned_in", route: "/orders"},
          {name: "Users", icon: "perm_identity", route: "/users"}];
}
