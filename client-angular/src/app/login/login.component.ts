import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-10%)', opacity: 0 }),
        animate('1000ms ease-in', style({transform: 'translateY(0%)', opacity: 1}))
      ]),
      transition(':leave', [
        animate('500ms ease-in', style({transform: 'translateY(-20%)'}))
      ])
    ])
  ]
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  error: string;
  visible: boolean = true;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required],
    });
  }

  onSubmit(loginData) {
    if (this.formGroup.valid) {
      this.error = "";
      this.authService.login(loginData.email, loginData.password).subscribe(data => {
        this.authService.saveUser(data);
        this.router.navigate(['']);
        this.visible = false;
      },
      err => {
        this.error = err.error.message;
      });
    }
  }

  hide = true;
}
