import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const USER_KEY = 'user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isUserLoggedIn: BehaviorSubject<boolean>;

  constructor(private http: HttpClient) {
    this.isUserLoggedIn = new BehaviorSubject<boolean>(this.getUser());
  }

  login(email: string, password: string) : Observable<any> {
    return this.http.post<any>(environment.loginUrl, {email: email, password: password});
  }

  saveUser(user: any) : void {
    localStorage.setItem(USER_KEY, JSON.stringify(user));
    this.isUserLoggedIn.next(true);
  }

  logout() : void {
    localStorage.removeItem(USER_KEY);
    this.isUserLoggedIn.next(false);
  }

  getUser() : any {
    const user = localStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return null;
  }

  isLoggedIn() : Observable<boolean> {
    (this.getUser()) ? this.isUserLoggedIn.next(true) : this.isUserLoggedIn.next(false);
    return this.isUserLoggedIn.asObservable();
  }
}
