import { animate, state, style, transition, trigger } from '@angular/animations';
import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { timer } from 'rxjs';
import { Product } from './models/product';
import { ProductsService } from './services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProductsComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'petType', 'type', 'price'];
  dataSource = new MatTableDataSource<Product>();
  expandedElement: Product | null;
  isImporting: boolean = false;

  constructor(private productService: ProductsService, 
              private router: Router,
              private toastr: ToastrService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  loadData(data?) {
    if(data) {
      this.dataSource = data;
    }
    else {
      this.productService.getAllProducts().subscribe(data => {
        this.dataSource.data = data;
      });
    }
  }

  addProduct() {
    this.router.navigate(['/products/add']);
  }

  onRefresh() {
    this.loadData();
  }

  onImport(){
    this.isImporting = true;
    this.productService.import().subscribe(data => {
      this.toastr.success(data.message, "Succeess");
      this.loadData();
      this.isImporting = false;
    })
  }
}
