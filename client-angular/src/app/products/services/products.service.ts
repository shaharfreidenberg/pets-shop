import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  getAllProducts() : Observable<Product[]>  {
    return this.http.get<Product[]>(environment.allProductsUrl);
  }

  getProductPicture(id: string): Observable<Blob> {
    return this.http.get(environment.productPicture + id, { responseType: 'blob' });
  }

  updateProduct(product: Product) : Observable<{message:string}> {
    return this.http.put<{message:string}>(environment.saveProduct, product);
  }

  uploadImage(id:string, image: File, isCreate: boolean) : Observable<{message:string}> {
    var formData = new FormData();
    formData.append("id", id);
    formData.append("isCreate", isCreate ? 'true' : 'false');
    formData.append("image", image);
    return this.http.put<{message:string}>(environment.productPicture, formData);
  }

  delete(id: string) : Observable<{message:string}>{
    return this.http.delete<{message:string}>(environment.products + id);
  }

  create(product: Product) : Observable<Product>{
    return this.http.post<Product>(environment.saveProduct, product);
  }

  filter(textFilter: string, petType: string, productType:string) : Observable<Product[]> {
    var params = new HttpParams().set("textFilter", textFilter).set("petType", petType).set("productType", productType)
    return this.http.get<Product[]>(environment.filterProducts, {params: params});
  }

  import() : Observable<{message:string}>{
    return this.http.get<{message:string}>(environment.productsImport);
  }
}
