import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductType } from '../models/productType';

@Injectable({
  providedIn: 'root'
})
export class ProductTypesService {

  constructor(private http: HttpClient) { }

  getTypeByPet(id: string) : Observable<ProductType[]>  {
    return this.http.get<ProductType[]>(environment.productTypeByPetUrl + id);
  }

  getAll() : Observable<ProductType[]> {
    return this.http.get<ProductType[]>(environment.allProductTypesUrl);
  }

  create(name: string, petType: string) : Observable<{message:string}> {
    return this.http.post<{message:string}>(environment.productTypesUrl, { name: name, petType: petType });
  }

  delete(id: string) : Observable<{message:string}>{
    return this.http.delete<{message:string}>(environment.productTypesUrl + id);
  }

  update(productType: ProductType) : Observable<{message:string}> {
    return this.http.put<{message:string}>(environment.productTypesUrl, productType);
  }
}
