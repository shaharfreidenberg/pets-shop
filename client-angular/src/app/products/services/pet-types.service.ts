import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PetType } from '../models/petType';

@Injectable({
  providedIn: 'root'
})
export class PetTypesService {

  constructor(private http: HttpClient) { }

  getAllPetTypes() : Observable<PetType[]>  {
    return this.http.get<PetType[]>(environment.petTypesUrl);
  }

  create(name: string) : Observable<{message:string}> {
    return this.http.post<{message:string}>(environment.petTypesUrl, { name: name });
  }

  delete(id: string) : Observable<{message:string}>{
    return this.http.delete<{message:string}>(environment.petTypesUrl + id);
  }

  update(petType: PetType) : Observable<{message:string}> {
    return this.http.put<{message:string}>(environment.petTypesUrl, petType);
  }
}
