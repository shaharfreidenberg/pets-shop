import { PetType } from "./petType";
import { ProductType } from "./productType";

export interface Product {
    _id: string;
    name: string;
    productType: ProductType;
    petType: PetType;
    supplier: string;
    picture: string;
    description: string;
    price: number;
}