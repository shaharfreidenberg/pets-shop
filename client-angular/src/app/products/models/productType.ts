import { PetType } from "./petType";

export interface ProductType {
    _id: string;
    name: string;
    petType: PetType;
  }