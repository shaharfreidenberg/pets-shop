import { Component } from '@angular/core';
import { RealtimeService } from './../realtime.service';
import { Order } from './order';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-order-notification',
  templateUrl: './order-notification.component.html',
  styleUrls: ['./order-notification.component.css']
})
export class OrderNotificationComponent {

  constructor(private service : RealtimeService, private toastr: ToastrService) {
    service.newOrder.subscribe(() => console.log('subscribed'));
    // service.newOrder.subscribe(order => {
    //   console.log(order)
    //   this.showNotification(order);
    // });
  }

  showNotification(order) {
    this.toastr.info('new order!', `new order has been placed by user ${order.msg}`);
  }
}
