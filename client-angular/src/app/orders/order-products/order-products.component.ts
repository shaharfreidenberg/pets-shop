import { Component, Input, OnInit } from '@angular/core';
import { Product} from '../../products/models/product';
import _ from 'lodash';

 @Component({
  selector: 'app-order-products',
  templateUrl: './order-products.component.html',
  styleUrls: ['./order-products.component.css']
})
export class OrderProductsComponent implements OnInit {
  @Input() products: Product[];
  displayedColumns: string[] = ['_id', 'name', 'price', 'amount', 'totalPrice'];
  dataSource = [];
  ngOnInit(): void {
    const productsGroupedById = Object.values(_.groupBy(this.products, '_id'));
    let list = [];
    productsGroupedById.map(group => {
       list.push(Object.assign({
        _id: group[0]._id,
        name: group[0].name,
        price: group[0].price,
        amount: (group as any).length,
        totalPrice: (group as any).map(p => p.price).reduce((sum, value) => sum + Number(value), 0)
      }));
    });
    this.dataSource = list;
  }
}