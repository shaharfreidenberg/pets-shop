import {animate, state, style, transition, trigger} from '@angular/animations';
import { OrdersService } from './orders.service';
import { Order } from './order';
import {AfterViewInit, Component, ViewChild, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { RealtimeService } from './../realtime.service';

@Component({
  selector: 'app-orders',
  styleUrls: ['./orders.component.css'],
  templateUrl: './orders.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class OrdersComponent implements OnInit {
  constructor(private realTimeService : RealtimeService, private ordersService: OrdersService) {
    realTimeService.newOrder.subscribe((orderId) => {
      this.ordersService.getOrderById(orderId).subscribe(order => {
        order.productsAmount = order.products.length;
        order.totalPrice = order.products.map(p => p.price).reduce((sum, value) => sum + Number(value), 0);
        this.dataSource.data = [order].concat(this.dataSource.data);
      });
    });
  }
  dataSource = new MatTableDataSource<Order>();
  columnsToDisplay = ['_id', 'date', 'userId', 'productsAmount', 'totalPrice', 'isShipped'];
  expandedElement: Order | null;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.ordersService.getOrders().subscribe(data => {
      data.map(d => {
        d.productsAmount = d.products.length;
        d.totalPrice = d.products.map(p => p.price).reduce((sum, value) => sum + Number(value), 0);
      });
      this.dataSource.data = data;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setOrderStatus(element) {
    this.ordersService.setOrderStatus(element._id, element.isShipped);
  }
}
