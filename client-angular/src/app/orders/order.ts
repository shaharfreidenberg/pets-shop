import { Product } from '../products/models/product';

export interface Order {
    _id: string;
    date: string;
    userId: string;
    productsAmount: number;
    totalPrice: number;
    isShipped: boolean;
    products: Product[];
  }