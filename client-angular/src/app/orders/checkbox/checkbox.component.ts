import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {
  @Input() checked: boolean;
  @Input() text: string;
  @Output() onChange = new EventEmitter();
  @Output() checkedChange = new EventEmitter();

  constructor() { }

  setChecked(checked) {
    this.checkedChange.emit(checked);
    this.onChange.emit();
  }
}
