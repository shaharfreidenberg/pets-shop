import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from './order';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  constructor(private http: HttpClient) { }

  getOrders() : Observable<Order[]>  {
    return this.http.get<Order[]>(environment.ordersUrl);
  }

  setOrderStatus(orderId: string, isShipped: boolean) {
    this.http.put(environment.orderStatusUrl, {orderId, isShipped}).subscribe(response => {});
  }
  
  getOrderById(orderId: string) {
    return this.http.get<Order>(`${environment.ordersUrl}/${orderId}`);
  }
}