import { animate, style, transition, trigger } from '@angular/animations';
import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { timer } from 'rxjs';
import { DialogComponent } from '../dialog/dialog.component';
import { PetType } from '../products/models/petType';
import { PetTypesService } from '../products/services/pet-types.service';

@Component({
  selector: 'app-pet-types',
  templateUrl: './pet-types.component.html',
  styleUrls: ['./pet-types.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ]),
    trigger('slideLeftRight', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-out', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class PetTypesComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'edit', 'delete'];
  dataSource = new MatTableDataSource<PetType>();
  visible: boolean = false;
  newName = new FormControl('', [Validators.required]);
  isEdit:boolean = false;
  formGroup: FormGroup;

  constructor(private petTypeService: PetTypesService,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private formBuilder: FormBuilder) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.loadData();
    this.createForm();
  }

  loadData() {
    this.petTypeService.getAllPetTypes().subscribe(data => this.dataSource.data = data);
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      'id': [{value: '', disabled: true}],
      'name': ['', Validators.required],
    });
  }

  onEditClick(petType: PetType) {
    this.isEdit = false;
    timer(500).subscribe(x => { 
      this.formGroup.setValue({ 
        id: petType._id,
        name: petType.name
      });
      this.formGroup.get('id').disable();
      this.isEdit = true 
    });
  }

  onCancelEditClick() {
    this.isEdit = false;
  }

  onSubmit(formData) {
    if(this.formGroup.valid) {
      this.petTypeService.update({
        _id: this.formGroup.get('id').value, 
        name: formData.name
      }).subscribe(data => {
          this.toastr.success(data.message, "Succeess");
          this.loadData();
          this.isEdit = false;
      },
      err => {
        this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}});
      });
    }
  }

  onDeleteClick(id: string) {
    const dialogRef = this.dialog.open(DialogComponent, {data: {
      title: "Warning", 
      message: `Are you sure you want to delete pet type ${id}? 
                Make sure that there are no products and products types that are related to this pet type, otherwise they will be deleted as well.`,
      isDelete: true}});

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.petTypeService.delete(id).subscribe(data => {
          this.toastr.success(data.message, "Succeess");
          this.loadData();
        },
        err => {
          this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}});
        });
      }
    });
  }

  addPetType() {
    this.visible = true;

    if(this.newName.valid  && this.visible) {
      this.petTypeService.create(this.newName.value).subscribe(data => {
        this.toastr.success(data.message, "Succeess");
        this.loadData();
        this.onCancelClick();
      },
      err => this.dialog.open(DialogComponent, {data: {title: "Error", message: err.error.message, isDelete: false}}));
    }
  }

  onCancelClick() {
    this.visible = false;
    this.newName.setValue('');
    this.newName.markAsUntouched();
  }

  onRefresh() {
    this.loadData();
  }

}
