import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { PetType } from '../products/models/petType';
import { Product } from '../products/models/product';
import { ProductType } from '../products/models/productType';
import { PetTypesService } from '../products/services/pet-types.service';
import { ProductTypesService } from '../products/services/product-types.service';
import { ProductsService } from '../products/services/products.service';

@Component({
  selector: 'app-products-filter',
  templateUrl: './products-filter.component.html',
  styleUrls: ['./products-filter.component.css']
})
export class ProductsFilterComponent implements OnInit {
  @Output() filterList = new EventEmitter();
  selectedPetType: string;
  selectedProductType: string;
  textFilter: string;
  productTypes: ProductType[];
  petTypes: PetType[];
  activeRequest;

  constructor(private productTypeService: ProductTypesService,
              private petTypeService: PetTypesService,
              private productService: ProductsService) { }

  ngOnInit(): void {
    this.petTypeService.getAllPetTypes().subscribe(data => this.petTypes = data);
    this.selectedPetType = "";
    this.selectedProductType = "";
    this.textFilter = "";
  }

  onFilter() {
    var requestId = this.textFilter + this.selectedPetType + this.selectedProductType;
    this.activeRequest = requestId;
    this.productService.filter(this.textFilter, this.selectedPetType, this.selectedProductType).subscribe(data => {
      if (this.activeRequest == requestId) {
        this.filterList.emit(data);
        this.activeRequest = undefined;
      }
    });
  }

  onPetChange(event: MatSelectChange) {
    if(event.value === "") {
      this.productTypes = [];
      this.selectedProductType = "";
    }
    else {
      this.productTypeService.getTypeByPet(event.value).subscribe(data => this.productTypes = data);
    }

    this.onFilter();
  }
}
