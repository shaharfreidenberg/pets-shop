export interface MonthlyIncomeId {
    month: string,
    year: string
}