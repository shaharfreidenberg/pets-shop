import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountersService {

  constructor(private http: HttpClient) { }
  getRegisteredUsersCount() : Observable<number>  {
    return this.http.get<number>(environment.registeredUsersCountUrl);
  }
  getProductsCount() : Observable<number>  {
    return this.http.get<number>(environment.productsCountUrl);
  }
  getOrdersCount() : Observable<number>  {
    return this.http.get<number>(environment.ordersCountUrl);
  }
}
