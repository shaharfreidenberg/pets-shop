import { Component, OnInit } from '@angular/core';
import { RealtimeService } from '../../realtime.service';
import { CountersService } from './counters.service';

@Component({
  selector: 'app-counters',
  templateUrl: './counters.component.html',
  styleUrls: ['./counters.component.css']
})
export class CountersComponent implements OnInit {
  connectedUsersCount: number;
  connectedUsersIcon: string;
  connectedUsersTitle: string;
  registeredUsersCount: number;
  registeredUsersIcon: string;
  registeredUsersTitle: string;
  productsCount: number;
  productsIcon: string;
  productsTitle: string;
  ordersCount: number;
  ordersIcon: string;
  ordersTitle: string;

  constructor(private servie : RealtimeService, private countersService: CountersService) {
    // connected users
    this.connectedUsersIcon = 'person';
    this.connectedUsersTitle = 'Connected';
    servie.connectedUsersCount.subscribe(count => this.connectedUsersCount = count);
    // registered users
    this.registeredUsersIcon = 'perm_identity';
    this.registeredUsersTitle = 'Registered';
    servie.newRegisteredUser.subscribe(() => this.registeredUsersCount += 1);
    // products
    this.productsIcon = 'pets';
    this.productsTitle = 'Products';
    servie.newProduct.subscribe(() => this.productsCount += 1);
    // orders
    this.ordersIcon = 'assignment_turned_in';
    this.ordersTitle = 'Orders';
    servie.newOrder.subscribe(() => this.ordersCount += 1);
  }

  ngOnInit(): void {
    this.getRegisteredUsersCount();
    this.getProductsCount();
    this.getOrdersCount();
  }
  
  getRegisteredUsersCount(){
    this.countersService.getRegisteredUsersCount().subscribe(data => {
      this.registeredUsersCount = data;
    });
  }
  getProductsCount(){
    this.countersService.getProductsCount().subscribe(data => {
      this.productsCount = data;
    });
  }
  getOrdersCount(){
    this.countersService.getOrdersCount().subscribe(data => {
      this.ordersCount = data;
    });
  }

}
