import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Directive, Input } from '@angular/core';
import * as d3 from 'd3';
import { MonthlyIncome } from './monthlyIncome';

@Directive({
  selector: '[appMonthlyIncomes]'
})
export class MonthlyIncomesDirective {
  private max = 0;
  @Input() data: MonthlyIncome[]
  ngOnInit(): void {
    this.max = Math.max(...this.data.map(x => x.income));
    this.data.forEach(d => d.date = `${d._id.month}/${d._id.year}`);
    this.createSvg();
    this.drawBars(this.data);
  }
private svg;
private margin = 50;
private width = 750 - (this.margin * 2);
private height = 400 - (this.margin * 2);

private createSvg(): void {
  this.svg = d3.select("div#bar")
  .append("svg")
  .attr("width", this.width + (this.margin * 2))
  .attr("height", this.height + (this.margin * 2))
  .append("g")
  .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
}
private drawBars(data: any[]): void {
  // Create the X-axis band scale
  const x = d3.scaleBand()
  .range([0, this.width])
  .domain(data.map(d => d.date))
  .padding(0.2);

  // Draw the X-axis on the DOM
  this.svg.append("g")
  .attr("transform", "translate(0," + this.height + ")")
  .call(d3.axisBottom(x))
  .selectAll("text")
  .attr("transform", "translate(-10,0)rotate(-45)")
  .style("text-anchor", "end");

  // Create the Y-axis band scale
  const y = d3.scaleLinear()
  .domain([0, this.max])
  .range([this.height, 0]);

  // Draw the Y-axis on the DOM
  this.svg.append("g")
  .call(d3.axisLeft(y));

  // Create and fill the bars
  this.svg.selectAll("bars")
  .data(data)
  .enter()
  .append("rect")
  .attr("x", d => x(d.date))
  .attr("y", d => y(d.income))
  .attr("width", x.bandwidth())
  .attr("height", (d) => this.height - y(d.income))
  .attr("fill", "#3f51b5");
}

}
