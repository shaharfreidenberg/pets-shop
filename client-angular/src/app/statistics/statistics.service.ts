import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { MonthlyIncome } from './monthlyIncome';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  constructor(private http: HttpClient) { }
  getMonthlyIncomes() : Observable<MonthlyIncome[]>  {
    return this.http.get<MonthlyIncome[]>(environment.statisticsUrl);
  }
}
