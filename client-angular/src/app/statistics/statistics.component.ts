import { Component, OnInit } from '@angular/core';
import { StatisticsService } from './statistics.service';
import { MonthlyIncome } from './monthlyIncome';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  monthlyIncomes: MonthlyIncome[];
  constructor(private statisticsService: StatisticsService) { }
  
  ngOnInit() {
    this.getMonthlyIncomes();
  }

  getMonthlyIncomes(){
    this.statisticsService.getMonthlyIncomes().subscribe(data => {
      this.monthlyIncomes = data;
    });
  }
}
