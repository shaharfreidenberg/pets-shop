import { MonthlyIncomeId } from './monthlyIncomeId';

export interface MonthlyIncome {
    _id: MonthlyIncomeId,
    income: number,
    date: string
}