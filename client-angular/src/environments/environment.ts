// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  statisticsUrl: 'http://localhost:8080/statistics/monthlyIncomes',
  registeredUsersCountUrl: 'http://localhost:8080/statistics/registeredUsersCount',
  productsCountUrl: 'http://localhost:8080/statistics/productsCount',
  ordersCountUrl: 'http://localhost:8080/statistics/ordersCount',
  ordersUrl: 'http://localhost:8080/orders',
  orderByIdUrl: 'http://localhost:8080/orders',
  orderStatusUrl: 'http://localhost:8080/orders/status',
  loginUrl: 'http://localhost:8080/users/admin/login',
  allUsersUrl: 'http://localhost:8080/users/all',
  changePermissionsUrl: 'http://localhost:8080/users/changePermissions',
  allProductsUrl: 'http://localhost:8080/products/all',
  productPicture: 'http://localhost:8080/products/picture/',
  productTypeByPetUrl: 'http://localhost:8080/productTypes/petType/',
  petTypesUrl: 'http://localhost:8080/petTypes/',
  saveProduct: 'http://localhost:8080/products',
  products: 'http://localhost:8080/products/',
  filterProducts: 'http://localhost:8080/products/filter',
  allProductTypesUrl: 'http://localhost:8080/productTypes/all',
  productTypesUrl: 'http://localhost:8080/productTypes/',
  productsImport: 'http://localhost:8080/products/import'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
